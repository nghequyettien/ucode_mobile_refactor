import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import 'package:ucode/app_state.dart';

class Global {

  static bool isDev = true;

  static Future init() async {
    setupUI();
    await setupGlobal();
  }

  static Future setupGlobal() async {
    WidgetsFlutterBinding.ensureInitialized();
    // SystemChrome.setSystemUIOverlayStyle(
    //   const SystemUiOverlayStyle(
    //     statusBarColor: Colors.transparent,
    //     systemNavigationBarColor: Colors.transparent,
    //     systemNavigationBarIconBrightness: Brightness.dark,
    //   ),
    // );
    await SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.leanBack,
    );

    Get.put<AppState>(AppState());
  }

  static void setupUI() {}
}
