import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/router/app_router.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/l10n/app_locale.dart';

extension ContextExtensions on BuildContext {
  /// return screen width
  double width() => MediaQuery.of(this).size.width;

  /// return screen height
  double height() => MediaQuery.of(this).size.height;

  // /// return screen size
  // Size size() => MediaQuery.of(this).size;
  //
  // /// return screen devicePixelRatio
  // double pixelRatio() => MediaQuery.of(this).devicePixelRatio;
  //
  // /// returns brightness
  // Brightness platformBrightness() => MediaQuery.of(this).platformBrightness;
  //
  // /// Return the height of status bar
  // double get statusBarHeight => MediaQuery.of(this).padding.top;
  //
  // /// Return the height of navigation bar
  // double get navigationBarHeight => MediaQuery.of(this).padding.bottom;

  /// Returns Theme.of(context)
  ThemeData get theme => Theme.of(this);

  // /// Returns Theme.of(context).textTheme
  // TextTheme get textTheme => Theme.of(this).textTheme;
  //
  // /// Returns DefaultTextStyle.of(context)
  // DefaultTextStyle get defaultTextStyle => DefaultTextStyle.of(this);
  //
  // /// Returns Form.of(context)
  // FormState? get form => Form.of(this);
  //
  // /// Returns Scaffold.of(context)
  // ScaffoldState get scaffold => Scaffold.of(this);
  //
  // /// Returns Overlay.of(context)
  // OverlayState? get overlay => Overlay.of(this);
  //
  // /// Returns primaryColor Color
  // Color get primaryColor => theme.primaryColor;
  //
  // /// Returns accentColor Color
  // Color get accentColor => theme.colorScheme.secondary;

  /// Returns scaffoldBackgroundColor Color
  Color get scaffoldBackgroundColor => theme.scaffoldBackgroundColor;

  /// Returns cardColor Color
  Color get cardColor => theme.cardColor;

  // /// Returns dividerColor Color
  // Color get dividerColor => theme.dividerColor;

  /// Returns dividerColor Color
  Color get iconColor => theme.iconTheme.color!;

  /// Return app style
  AppStyle get appStyle => Provider.of<AppStyle>(this, listen: false);
  AppStyle get appStyleListen => Provider.of<AppStyle>(this, listen: true);

  /// Return app locale
  AppLocale get appLocale => Provider.of<AppLocale>(this, listen: false);

  /// Return app router
  AppRouter get appRouter => Provider.of<AppRouter>(this, listen: false);

  /// Return AppLocalizations
  AppLocalizations? get lang => AppLocalizations.of(this);

  // /// Request focus to given FocusNode
  // void requestFocus(FocusNode focus) {
  //   FocusScope.of(this).requestFocus(focus);
  // }
}
