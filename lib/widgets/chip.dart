import 'package:flutter/material.dart';
import 'package:ucode/styles/styles.dart';

// ignore: non_constant_identifier_names
Widget ChipWidget(String text) {
  return Container(
    padding: EdgeInsets.symmetric(
      horizontal: Insets.sm,
      vertical: Insets.xs,
    ),
    decoration: BoxDecoration(
      color: $appColorPrimary,
      borderRadius: Corners.xxlBorder,
    ),
    child: Text(
      text,
      style: TextStyles.body2.copyWith(
        color: $whiteColor,
        height: 0,
      ),
    ),
  );
}

// ignore: non_constant_identifier_names
Widget ChipOutLineWidget(String text) {
  return Container(
    padding: EdgeInsets.symmetric(
      horizontal: Insets.sm - 1,
      vertical: Insets.xs - 1,
    ),
    decoration: BoxDecoration(
      color: $green100,
      borderRadius: Corners.xxlBorder,
      border: Border.all(
        color: $appColorPrimary,
        width: Strokes.thin,
      ),
    ),
    child: Text(
      text,
      style: TextStyles.body2.copyWith(
        color: $appColorPrimary,
        height: 0,
      ),
    ),
  );
}
