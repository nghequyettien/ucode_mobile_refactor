import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/constants/images.dart';

import 'package:cached_network_image/cached_network_image.dart';

// ignore: non_constant_identifier_names
Widget ImageAssetWidget(
  String path, {
  double? width,
  double? height,
}) {
  return SizedBox(
    height: height ?? 100 * ConfigScale().scale,
    width: width ?? 100 * ConfigScale().scale,
    child: Image.asset(path),
  );
}

// ignore: non_constant_identifier_names
Widget CachedNetworkImageWidget(
  String? url, {
  double? width,
  double? height,
  BoxFit? boxFit,
}) {
  if (url == null) {
    return Image.asset(
      APP_ICON,
      fit: boxFit ?? BoxFit.cover,
      height: height,
      width: width,
    );
  }
  return CachedNetworkImage(
    imageUrl: url,
    fit: boxFit ?? BoxFit.cover,
    imageBuilder: (context, imageProvider) {
      return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: boxFit ?? BoxFit.cover,
          ),
        ),
      );
    },
    errorWidget: (context, error, e) {
      return Image.asset(
        DEFAULT_BANNER,
        fit: boxFit ?? BoxFit.cover,
        height: height,
        width: width,
      );
    },
    progressIndicatorBuilder: (context, error, e) {
      return Shimmer.fromColors(
        baseColor: Colors.grey.shade400,
        highlightColor: Colors.grey.shade100,
        child: Container(
          width: width,
          height: height,
          decoration: const BoxDecoration(
            color: $textSecondaryColor,
          ),
        ),
      );
    },
  );
}

// ignore: non_constant_identifier_names
Widget CachedNetworkImageCircleWidget(
  String? url, {
  double? size,
}) {
  if (url == null) {
    return CircleAvatar(
      backgroundImage: const AssetImage(APP_ICON),
      radius: size,
    );
  }
  return CachedNetworkImage(
    imageUrl: url,
    fit: BoxFit.cover,
    imageBuilder: (context, imageProvider) {
      return CircleAvatar(
        backgroundImage: imageProvider,
        radius: size,
      );
    },
    errorWidget: (context, error, e) {
      return CircleAvatar(
        backgroundImage: const AssetImage(APP_ICON),
        radius: size,
      );
    },
    progressIndicatorBuilder: (context, error, e) {
      return Shimmer.fromColors(
        baseColor: Colors.grey.shade400,
        highlightColor: Colors.grey.shade100,
        child: Container(
          width: size! * 2,
          height: size * 2,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: $textSecondaryColor,
          ),
        ),
      );
    },
  );
}
