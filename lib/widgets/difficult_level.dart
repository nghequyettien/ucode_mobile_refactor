import 'package:flutter/material.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/constants/variables.dart';
import 'package:ucode/extensions/extensions.dart';

Widget difficultLevelWidget(BuildContext context, String difficultLevel) {
  switch (difficultLevel) {
    case $trivial:
      return Text(
        '${context.lang!.difficult_level}: ${context.lang!.trivial}',
        style: TextStyles.body2.copyWith(color: $textSecondaryColor),
      );
    case $medium:
      return Row(
        children: <Widget>[
          Text(
            '${context.lang!.difficult_level}: ',
            style: TextStyles.body2.copyWith(color: $textSecondaryColor),
          ),
          Text(
            context.lang!.medium,
            style: TextStyles.body2.copyWith(color: $warnColor),
          ),
        ],
      );
    case $hard:
      return Row(
        children: <Widget>[
          Text(
            '${context.lang!.difficult_level}: ',
            style: TextStyles.body2.copyWith(color: $textSecondaryColor),
          ),
          Text(
            context.lang!.hard,
            style: TextStyles.body2.copyWith(color: $errorColorDark),
          ),
        ],
      );
    case $easy:
      return Row(
        children: <Widget>[
          Text(
            '${context.lang!.difficult_level}: ',
            style: TextStyles.body2.copyWith(color: $textSecondaryColor),
          ),
          Text(
            context.lang!.easy,
            style: TextStyles.body2.copyWith(color: $appColorPrimary),
          ),
        ],
      );
    default:
      return Text(
        '${context.lang!.difficult_level}: ${context.lang!.trivial}',
        style: TextStyles.body2.copyWith(color: $textSecondaryColor),
      );
  }
}
