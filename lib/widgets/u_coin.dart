import 'package:flutter/material.dart';

import 'package:ucode/utils/utils.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/images.dart';

class UCoinWidget extends StatelessWidget {
  const UCoinWidget({
    Key? key,
    required this.uCoin,
    this.iconIsRight = false,
    this.isStart = true,
  }) : super(key: key);

  final int? uCoin;
  final bool iconIsRight;
  final bool isStart;

  @override
  Widget build(BuildContext context) {
    final double iconSize = 30 * ConfigScale().scale;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:
          isStart ? MainAxisAlignment.start : MainAxisAlignment.end,
      children: <Widget>[
        if (!iconIsRight)
          Image.asset(
            APP_UCOIN,
            width: iconSize,
            height: iconSize,
          ),
        if (!iconIsRight) HSpace.xs,
        Flexible(
          child: Text(
            truncateNumber(uCoin ?? 0),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyles.title1,
          ),
        ),
        if (iconIsRight) HSpace.xs,
        if (iconIsRight)
          Image.asset(
            APP_UCOIN,
            width: iconSize,
            height: iconSize,
          ),
      ],
    );
  }
}
