import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:ucode/styles/styles.dart';

class SocialIcon extends StatelessWidget {
  const SocialIcon({
    Key? key,
    required this.backgroundColor,
    required this.iconPath,
    this.size = 40,
  }) : super(key: key);

  final Color backgroundColor;
  final String iconPath;
  final double size;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: backgroundColor,
      ),
      width: size,
      height: size,
      child: Padding(
        padding: EdgeInsets.all(Insets.sm),
        child: SvgPicture.asset(
          iconPath,
          color: $whiteColor,
        ),
      ),
    );
  }
}
