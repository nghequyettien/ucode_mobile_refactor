import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:ucode/constants/images.dart';
import 'package:ucode/extensions/extensions.dart';
import 'package:ucode/helpers/helpers.dart';
import 'package:ucode/styles/styles.dart';

class CachedImageWidget extends StatefulWidget {
  const CachedImageWidget({
    Key? key,
    required this.imageUrl,
    required this.height,
    required this.width,
    this.isBanner = false,
  }) : super(key: key);

  final String imageUrl;
  final double height;
  final double width;
  final bool isBanner;

  @override
  State<CachedImageWidget> createState() => _CachedImageWidgetState();
}

class _CachedImageWidgetState extends State<CachedImageWidget> {
  late final Future<String> getImage;
  late String _base64;

  @override
  void initState(){
    super.initState();
    getImage = _getImage();
  }

  Future<String> _getImage() async {
    String base64 = await CustomSharedPreferences.get(widget.imageUrl);
    print(base64);
    if (base64.isEmpty) {
      print('end code');
      final http.Response response = await http.get(Uri.parse(widget.imageUrl));
      base64 = base64Encode(response.bodyBytes);
      CustomSharedPreferences.set(widget.imageUrl, base64);
    }
    _base64 = base64;
    return base64;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: getImage,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (!_base64.isEmptyOrNull) {
            _base64 = snapshot.data!;
            final Uint8List bytes = base64Decode(_base64);
            return Image.memory(
              bytes,
              fit: BoxFit.cover,
              height: widget.height,
              width: widget.width,
            );
          }
          return Image.asset(
            widget.isBanner ? DEFAULT_BANNER : APP_ICON,
            fit: BoxFit.cover,
            height: widget.height,
            width: widget.width,
          );
        }
        return Shimmer.fromColors(
          baseColor: Colors.grey.shade400,
          highlightColor: Colors.grey.shade100,
          child: Container(
            width: widget.width,
            height: widget.height,
            decoration: BoxDecoration(
              color: $shadowColorGlobal,
            ),
          ),
        );
      },
    );
  }
}
