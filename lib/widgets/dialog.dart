// import 'package:get/get.dart';
// import 'package:flutter/material.dart';
// import 'package:ucode/extensions/extensions.dart';
//
// import 'package:ucode/styles/styles.dart';
//
// class CustomDialogWarning extends StatelessWidget {
//   CustomDialogWarning({
//     Key? key,
//     required this.message,
//     this.bgColor,
//     this.icon = Icons.error_outline_rounded,
//     this.maxWidth,
//   }) : super(key: key);
//
//   final String message;
//   final Color? bgColor;
//   final IconData icon;
//   final double? maxWidth;
//
//   static final _defaultMaxWidth = 400 * ConfigScale().configCale;
//   final AppStyle _appStyle = AppStyle.to;
//
//   @override
//   Widget build(BuildContext context) {
//     return Obx((){
//       return Dialog(
//         elevation: 0.0,
//         backgroundColor: Colors.transparent,
//         child: Container(
//           decoration: BoxDecoration(
//             color: context.cardColor,
//             shape: BoxShape.rectangle,
//             borderRadius: Corners.medBorder,
//             boxShadow: Shadows.universal,
//           ),
//           width: maxWidth != null
//               ? context.width > maxWidth!
//               ? maxWidth
//               : context.width
//               : context.width > _defaultMaxWidth
//               ? _defaultMaxWidth
//               : context.width,
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               Container(
//                 width: double.infinity,
//                 padding: EdgeInsets.all(Insets.med),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.only(
//                     topRight: Corners.medRadius,
//                     topLeft: Corners.medRadius,
//                   ),
//                   color: bgColor ??
//                       (_appStyle.isLightMode ? $errorColorLight : $errorColorDark),
//                 ),
//                 child: Icon(
//                   icon,
//                   color: $whiteColor,
//                   size: IconSizes.med,
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.all(Insets.med),
//                 child: Text(
//                   message,
//                   style: TextStyles.body1.copyWith(
//                     color: _appStyle.isLightMode ? $textPrimaryColor : $whiteColor,
//                     height: 1,
//                   ),
//                   maxLines: 2,
//                 ),
//               ),
//             ],
//           ),
//         ),
//       );
//     });
//   }
// }
