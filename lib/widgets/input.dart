import 'package:flutter/material.dart';
import 'package:ucode/extensions/extensions.dart';

import 'package:ucode/styles/styles.dart';

class InputEditTextStyle extends StatelessWidget {
  const InputEditTextStyle({
    Key? key,
    this.hintText = '',
    this.isPassword = false,
    this.controller,
    this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  final String? hintText;
  final bool isPassword;
  final TextEditingController? controller;
  final Widget? prefixIcon;
  final Widget? suffixIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: boxDecoration(
        radius: Corners.med,
        showShadow: true,
        bgColor: $whiteColor,
      ),
      child: TextFormField(
        controller: controller,
        style: TextStyles.input1.copyWith(
          color: context.appStyle.isLightMode ? $textPrimaryColor : $whiteColor,
        ),
        obscureText: isPassword,
        decoration: InputDecoration(
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          contentPadding: EdgeInsets.symmetric(
            horizontal: Insets.xl,
            vertical: Insets.lg,
          ),
          hintText: hintText,
          hintStyle: TextStyles.input1.copyWith(
            color:
                context.appStyle.isLightMode ? $textPrimaryColor : $whiteColor,
          ),
          filled: true,
          fillColor:
              context.appStyle.isLightMode ? $whiteColor : $cardDarkColor,
          enabledBorder: OutlineInputBorder(
            borderRadius: Corners.medBorder,
            borderSide: const BorderSide(
              color: $whiteColor,
              width: 0.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: Corners.medBorder,
            borderSide: const BorderSide(
              color: $whiteColor,
              width: 0.0,
            ),
          ),
        ),
      ),
    );
  }
}
