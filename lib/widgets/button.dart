import 'package:flutter/material.dart';
import 'package:ucode/extensions/extensions.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';

class ButtonFullColor extends StatelessWidget {
  const ButtonFullColor({
    Key? key,
    required this.textContent,
    required this.onPressed,
  }) : super(key: key);

  final String? textContent;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(borderRadius: Corners.medBorder),
        padding: EdgeInsets.zero,
        elevation: Strokes.thick / 2,
        primary: $appColorPrimary,
        textStyle: const TextStyle(color: $whiteColor),
      ),
      onPressed: onPressed,
      child: Container(
        decoration: BoxDecoration(
          gradient: const LinearGradient(
            colors: <Color>[$appColorPrimary, $colorGradient1],
          ),
          borderRadius: Corners.medBorder,
        ),
        padding: EdgeInsets.zero,
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Insets.xl,
              vertical: Insets.lg,
            ),
            child: Text(
              textContent ?? '',
              style: TextStyles.button1.copyWith(
                color: $whiteColor,
                height: 1,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonFullColorWithIcon extends StatelessWidget {
  const ButtonFullColorWithIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ButtonOutLine extends StatelessWidget {
  const ButtonOutLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ButtonOutLineWithIcon extends StatelessWidget {
  const ButtonOutLineWithIcon({
    Key? key,
    this.iconIsRight = false,
    required this.icon,
    required this.textContent,
    required this.onPressed,
  }) : super(key: key);

  final bool iconIsRight;
  final Widget icon;
  final String textContent;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: $dividerColorLight, width: Strokes.thin),
          borderRadius: Corners.medBorder,
        ),
        padding: EdgeInsets.zero,
        elevation: Strokes.thick / 2,
        primary: $appColorPrimary,
        textStyle: TextStyle(
          color: context.appStyleListen.isLightMode
              ? $textPrimaryColor
              : $whiteColor,
        ),
      ),
      onPressed: onPressed,
      child: Container(
        decoration: BoxDecoration(
          color: $whiteColor,
          borderRadius: Corners.medBorder,
        ),
        padding: EdgeInsets.zero,
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Insets.xl,
              vertical: Insets.lg,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (!iconIsRight) icon,
                if (!iconIsRight) HSpace.med,
                Text(
                  textContent,
                  style: TextStyles.button1.copyWith(
                    color: context.appStyleListen.isLightMode
                        ? $textPrimaryColor
                        : $whiteColor,
                    height: 1,
                  ),
                  textAlign: TextAlign.center,
                ),
                if (iconIsRight) HSpace.med,
                if (iconIsRight) icon,
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonOutLineWithIconFlexible extends StatelessWidget {
  const ButtonOutLineWithIconFlexible({
    Key? key,
    this.iconIsRight = false,
    required this.icon,
    required this.textContent,
    required this.onPressed,
  }) : super(key: key);

  final bool iconIsRight;
  final Widget icon;
  final String textContent;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: $dividerColorLight, width: Strokes.thin),
          borderRadius: Corners.medBorder,
        ),
        padding: EdgeInsets.zero,
        elevation: Strokes.thin,
        textStyle: TextStyle(
          color: context.appStyleListen.isLightMode
              ? $textPrimaryColor
              : $whiteColor,
        ),
      ),
      onPressed: onPressed,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color:
              context.appStyle.isLightMode ? $whiteColor : $cardDarkColor,
          borderRadius: Corners.medBorder,
        ),
        padding: EdgeInsets.zero,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Insets.xl,
            vertical: Insets.lg,
          ),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Text(
                textContent,
                style: TextStyles.button1.copyWith(
                  color: context.appStyleListen.isLightMode
                      ? $textPrimaryColor
                      : $whiteColor,
                  height: 1,
                ),
                textAlign: TextAlign.center,
              ),
              Positioned(
                top: 0,
                bottom: 0,
                left: iconIsRight ? null : 0,
                right: iconIsRight ? 0 : null,
                child: icon.center(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
