import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/extensions/extensions.dart';

void toasty(
  BuildContext context,
  String text, {
  ToastGravity? gravity,
  length = Toast.LENGTH_SHORT,
  Color? bgColor,
  Color? textColor,
  bool print = false,
  bool removeQueue = false,
  Duration? duration,
  BorderRadius? borderRadius,
  EdgeInsets? padding,
}) {
  FToast().init(context);
  if (removeQueue) FToast().removeCustomToast();

  FToast().showToast(
    child: Container(
      decoration: BoxDecoration(
        color: bgColor ?? $appColorPrimary,
        boxShadow: defaultBoxShadow(),
        borderRadius: borderRadius ?? Corners.xxlBorder,
      ),
      padding: padding ??
          EdgeInsets.symmetric(
            vertical: Insets.med,
            horizontal: Insets.xl,
          ),
      child: Text(
        text,
        style: TextStyles.title2.copyWith(
          color: $whiteColor,
          height: 0,
        ),
      ),
    ),
    gravity: gravity ?? ToastGravity.BOTTOM,
    toastDuration: duration,
  );
  if (print) log(text);
}

void toastySuccess(BuildContext context, String text) {
  toasty(context, text, bgColor: $appColorPrimary);
}

void toastyError(BuildContext context, String text) {
  toasty(
    context,
    text,
    bgColor: context.appStyle.isLightMode ? $errorColorLight : $errorColorLight,
  );
}
