import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/pages/pages.dart';
import 'package:ucode/router/router.dart';

class AppRouter {
  AppRouter();

  final AppState appState = AppState.to;

  String _firstPageGo = '';

  String get firstPageGo => _firstPageGo;

  late final router = GoRouter(
    debugLogDiagnostics: true,
    urlPathStrategy: UrlPathStrategy.path,
    initialLocation: AppRouterPath.$rootPath,
    redirect: (state) {
      if (firstPageGo.isEmpty &&
          state.location != AppRouterPath.$rootPath &&
          state.location != AppRouterPath.$splashPath) {
        _firstPageGo = state.location;
      }
      if (state.location == AppRouterPath.$splashPath) return null;
      if (!appState.splashFinished) return AppRouterPath.$splashPath;

      if (!appState.isSignIn &&
          (state.location == AppRouterPath.$signInPath ||
              state.location == AppRouterPath.$signUpPath)) return null;
      if (!appState.isSignIn) return AppRouterPath.$signInPath;

      if (state.location == AppRouterPath.$signInPath ||
          state.location == AppRouterPath.$signUpPath) {
        return AppRouterPath.$homePath;
      }
      return null;
    },
    routes: [
      GoRoute(
        name: AppRouterName.$home,
        path: AppRouterPath.$homePath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$home),
              initPath: AppRouterPath.$homePath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$splash,
        path: AppRouterPath.$splashPath,
        pageBuilder: (context, state) => MaterialPage<void>(
          key: state.pageKey,
          child: const SplashScreen(),
        ),
      ),
      GoRoute(
        name: AppRouterName.$signIn,
        path: AppRouterPath.$signInPath,
        pageBuilder: (context, state) => MaterialPage<void>(
          key: state.pageKey,
          child: const SignInPage(),
        ),
      ),
      GoRoute(
        name: AppRouterName.$signUp,
        path: AppRouterPath.$signUpPath,
        pageBuilder: (context, state) => MaterialPage<void>(
          key: state.pageKey,
          child: const SignUpPage(),
        ),
      ),
      GoRoute(
        name: AppRouterName.$course,
        path: AppRouterPath.$coursePath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$course),
              initPath: AppRouterPath.$coursePath,
            ),
          );
        },
        routes: [
          GoRoute(
            name: AppRouterName.$courseDetail,
            path: ':courseId',
            pageBuilder: (context, state) => MaterialPage<void>(
              key: state.pageKey,
              child: CourseDetailPage(courseId: state.params['courseId']!),
            ),
          ),
        ],
      ),
      GoRoute(
        name: AppRouterName.$contest,
        path: AppRouterPath.$contestPath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$contest),
              initPath: AppRouterPath.$contestPath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$compete,
        path: AppRouterPath.$competePath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$compete),
              initPath: AppRouterPath.$competePath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$codeIde,
        path: AppRouterPath.$codeIdePath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$codeIde),
              initPath: AppRouterPath.$codeIdePath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$challenge,
        path: AppRouterPath.$challengePath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$challenge),
              initPath: AppRouterPath.$challengePath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$blog,
        path: AppRouterPath.$blogPath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$blog),
              initPath: AppRouterPath.$blogPath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$leaderboard,
        path: AppRouterPath.$leaderboardPath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$leaderboard),
              initPath: AppRouterPath.$leaderboardPath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$myCourse,
        path: AppRouterPath.$myCoursePath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$myCourse),
              initPath: AppRouterPath.$myCoursePath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$problem,
        path: AppRouterPath.$problemPath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$problem),
              initPath: AppRouterPath.$problemPath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$search,
        path: AppRouterPath.$searchPath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$search),
              initPath: AppRouterPath.$searchPath,
            ),
          );
        },
      ),
      GoRoute(
        name: AppRouterName.$setting,
        path: AppRouterPath.$settingPath,
        pageBuilder: (context, state) {
          return MaterialPage<void>(
            key: state.pageKey,
            child: const RedirectPage(
              key: Key(AppRouterName.$setting),
              initPath: AppRouterPath.$settingPath,
            ),
          );
        },
      ),
    ],
    errorPageBuilder: (context, state) {
      // TODO 404
      return MaterialPage<void>(
        key: state.pageKey,
        child: ErrorBuilderPage(
          error: state.error,
        ),
      );
    },
  );
}
