class AppRouterPath {
  static const String $rootPath = '/';
  static const String $homePath = '/';
  static const String $splashPath = '/splash';
  static const String $signInPath = '/sign-in';
  static const String $signUpPath = '/sign-up';
  static const String $coursePath = '/course';
  static const String $searchPath = '/search';
  static const String $problemPath = '/problem';
  static const String $blogPath = '/blog';
  static const String $competePath = '/compete';
  static const String $leaderboardPath = '/leaderboard';
  static const String $myCoursePath = '/my_course';
  static const String $settingPath = '/setting';
  static const String $challengePath = '/challenge';
  static const String $contestPath = '/contest';
  static const String $codeIdePath = '/code_ide';

  static String convertRouterPathToRouterName(String routerPath) {
    switch (routerPath) {
      case AppRouterPath.$homePath:
        return AppRouterName.$home;
      case AppRouterPath.$searchPath:
        return AppRouterName.$search;
      case AppRouterPath.$leaderboardPath:
        return AppRouterName.$leaderboard;
      case AppRouterPath.$myCoursePath:
        return AppRouterName.$myCourse;
      case AppRouterPath.$settingPath:
        return AppRouterName.$setting;
      case AppRouterPath.$coursePath:
        return AppRouterName.$course;
      case AppRouterPath.$problemPath:
        return AppRouterName.$problem;
      case AppRouterPath.$blogPath:
        return AppRouterName.$blog;
      case AppRouterPath.$competePath:
        return AppRouterName.$compete;
      case AppRouterPath.$challengePath:
        return AppRouterName.$challenge;
      case AppRouterPath.$contestPath:
        return AppRouterName.$contest;
      case AppRouterPath.$codeIdePath:
        return AppRouterName.$codeIde;
      default:
        return '';
    }
  }
}

class AppRouterName {
  static const String $root = 'root';
  static const String $home = 'home';
  static const String $splash = 'splash';
  static const String $signIn = 'signIn';
  static const String $signUp = 'signUp';
  static const String $course = 'course';
  static const String $courseDetail = 'courseDetail';
  static const String $search = 'search';
  static const String $problem = 'problem';
  static const String $blog = 'blog';
  static const String $compete = 'compete';
  static const String $leaderboard = 'leaderboard';
  static const String $myCourse = 'myCourse';
  static const String $setting = 'setting';
  static const String $challenge = 'challenge';
  static const String $contest = 'contest';
  static const String $codeIde = 'codeIde';

  static String convertRouterNameToRouterPath(String routerName) {
    switch (routerName) {
      case AppRouterName.$home:
        return AppRouterPath.$homePath;
      case AppRouterName.$search:
        return AppRouterPath.$searchPath;
      case AppRouterName.$leaderboard:
        return AppRouterPath.$leaderboardPath;
      case AppRouterName.$myCourse:
        return AppRouterPath.$myCoursePath;
      case AppRouterName.$setting:
        return AppRouterPath.$settingPath;
      case AppRouterName.$course:
        return AppRouterPath.$coursePath;
      case AppRouterName.$problem:
        return AppRouterPath.$problemPath;
      case AppRouterName.$blog:
        return AppRouterPath.$blogPath;
      case AppRouterName.$compete:
        return AppRouterPath.$competePath;
      case AppRouterName.$challenge:
        return AppRouterPath.$challengePath;
      case AppRouterName.$contest:
        return AppRouterPath.$contestPath;
      case AppRouterName.$codeIde:
        return AppRouterPath.$codeIdePath;
      default:
        return '';
    }
  }
}
