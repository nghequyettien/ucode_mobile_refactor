import 'dart:async';
import 'package:get/state_manager.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import 'package:ucode/helpers/helpers.dart';
import 'package:ucode/extensions/extensions.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';
import 'package:ucode/constants/constants.dart';

class AppState extends GetxController {

  static AppState get to => Get.find();

  final RxBool _splashFinished = RxBool(false);
  final RxBool _isSignIn = RxBool(false);
  final  RxString _token = RxString('');
  final Rx<User> _user = Rx<User>(User.empty);

  final GoogleSignIn _googleSignIn = GoogleSignIn();

  bool get splashFinished => _splashFinished.value;

  bool get isSignIn => _isSignIn.value;

  String get token => _token.value;

  User get user => _user.value;


  Future _updateUser() async {
    final User resUser = await UserEntities.getInfoUser();
    _user.value = resUser;
  }

  Future loadInfoUserFromLocalStorage() async {
    final String getToken = await CustomSharedPreferences.get($userToken);
    if (getToken.isEmptyOrNull){
      _splashFinished.value = true;
      return;
    }
    _token.value = getToken;
    try {
      await _updateUser();
    } catch (e) {
      _user.value = User.empty;
      CustomSharedPreferences.delete($userToken);
    }
    if (user.isNotEmpty) {
      _token.value = getToken;
      CustomSharedPreferences.set($userToken, getToken);
      _isSignIn.value = true;
    }
    _splashFinished.value = true;
  }

  Future signIn({
    required String email,
    required String password,
  }) async {
    final String resToken = await UserEntities.signIn(email, password);
    if(resToken.isEmpty) return;
    _token.value = resToken;
    CustomSharedPreferences.set($userToken, resToken);
    await _updateUser();
    if (user.isNotEmpty) {
      _isSignIn.value = true;
    }
  }

  Future signInWithGoogle() async {}

  Future signUp({
    required String email,
    required String password,
  }) async {}

  void logout() {
    ApiServices.logout();
    _isSignIn.value = false;
    _token.value = '';
    _user.value = User.empty;
    CustomSharedPreferences.delete($userToken);
  }
}
