import 'main_styles.dart';

enum ScreenWidth {
  MOBILE,
  TABLET,
  DESKTOP,
}

class Screen {
  static const double _desktop = 1200;
  static const double _tablet = 700;
  static const double _mobile = 300;

  static ScreenWidth getCurrentScreen(double width) {
    if (width > _desktop) {
      return ScreenWidth.DESKTOP;
    } else if (width > _tablet) {
      return ScreenWidth.TABLET;
    }
    return ScreenWidth.MOBILE;
  }
}

const List<double> _scales = [1, 1.05, 1.1];

void onChangeScreen(double width) {
  final ScreenWidth oldScreenWidth = ConfigScale().screenWidth;
  ConfigScale().setScreenWidth(Screen.getCurrentScreen(width));
  if (oldScreenWidth != ConfigScale().screenWidth) {
    switch (ConfigScale().screenWidth) {
      case ScreenWidth.DESKTOP:
        ConfigScale().setConfigScale(_scales[2]);
        break;
      case ScreenWidth.TABLET:
        ConfigScale().setConfigScale(_scales[1]);
        break;
      default:
        ConfigScale().setConfigScale(_scales[0]);
    }
  }
}
