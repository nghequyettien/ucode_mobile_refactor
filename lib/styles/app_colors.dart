import 'package:flutter/material.dart';

// Common Colors
const Color $whiteColor = Colors.white;
const Color $blackColor = Colors.black;

const Color $textPrimaryColor = Color(0xFF2E3033);
const Color $textSecondaryColor = Color(0xFF757575);

const Color $appColorPrimary = Color(0xFF20B149);
const Color $green100 = Color(0xFFE9F7ED);
const Color $colorGradient1 = Color(0xFF23E251);
// const Color $shadowColor = Color(0X95E9EBF0);

const Color $appSplashSecondaryColor = Color(0xFFD7DBDD);

const Color $warnColor = Color(0xFFFFB822);

// Light Theme Colors
const Color $errorColorLight = Colors.red;
const Color $hoverColorLight = Colors.white54;
const Color $viewLineColor = Color(0xFFEAEAEA);
const Color $dividerColorLight = $viewLineColor;
const Color $appSecondaryBackgroundColor = Color(0xff343434);

// Dark Theme Colors
const Color $errorColorDark = Color(0xFFCF6676);
const Color $appBackgroundColorDark = Color(0xFF121212);
const Color $cardBackgroundBlackDark = Color(0xFF1F1F1F);
const Color $colorPrimaryBlack = Color(0xFF131d25);
const Color $hoverColorDark = Colors.black12;
const Color $cardDarkColor = Color(0xFF12181B);
// const Color $scaffoldDarkColor = Color(0xFF1C1F26);

Color $shadowColorGlobal = Colors.grey.withOpacity(0.2);
Color $dividerColorDark = const Color(0xFFDADADA).withOpacity(0.3);
const Color $lightGray = Color(0xFFD3D3D3);
const Color $iconColorSecondary = Color(0xFF778390);