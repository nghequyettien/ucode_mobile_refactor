import 'package:flutter/material.dart';

import 'package:ucode/constants/constants.dart';
import 'package:ucode/extensions/extensions.dart';
import 'package:ucode/helpers/helpers.dart';
import 'package:ucode/router/router.dart';

import 'app_themes.dart';

enum AppThemeMode {
  light,
  dark,
}

class AppStyle extends ChangeNotifier {
  AppThemeMode _appThemeMode = AppThemeMode.light;

  AppThemeMode get appThemeMode => _appThemeMode;

  ThemeData get themeMode =>
      isLightMode ? AppThemeData.lightTheme : AppThemeData.darkTheme;

  bool get isLightMode => appThemeMode == AppThemeMode.light;

  Future _loadThemeFromLocalStorage() async {
    final String theme = await CustomSharedPreferences.get($appTheme);
    if (theme == AppThemeMode.dark.toString()) {
      _appThemeMode = AppThemeMode.dark;
      notifyListeners();
    }
  }

  AppStyle() {
    _loadThemeFromLocalStorage();
  }

  void setAppThemeMode(AppThemeMode themMode) {
    _appThemeMode = themMode;
    CustomSharedPreferences.set($appTheme, themMode.toString());
    notifyListeners();
  }
}
