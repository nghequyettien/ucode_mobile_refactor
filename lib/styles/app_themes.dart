import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_colors.dart';
import 'main_styles.dart';

class AppThemeData {
  AppThemeData._();

  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: $whiteColor,
    primaryColor: $appColorPrimary,
    primaryColorDark: $appColorPrimary,
    errorColor: $errorColorLight,
    hoverColor: $hoverColorLight,
    dividerColor: $dividerColorLight,
    fontFamily: Fonts.googleSans,
    appBarTheme: AppBarTheme(
      color: $whiteColor,
      iconTheme: const IconThemeData(color: $textPrimaryColor),
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.dark,
      ),
      titleTextStyle: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: FontSizes.s20,
        height: 1,
        color: $blackColor,
      ),
    ),
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: $appColorPrimary,
      selectionColor: $appColorPrimary,
      selectionHandleColor: $appColorPrimary,
    ),
    colorScheme: const ColorScheme.light(
      primary: $appColorPrimary,
    ),
    cardTheme: const CardTheme(color: $whiteColor),
    cardColor: $whiteColor,
    iconTheme: const IconThemeData(color: $textPrimaryColor),
    bottomSheetTheme: const BottomSheetThemeData(backgroundColor: $whiteColor),
    textTheme: const TextTheme(
      button: TextStyle(color: $appColorPrimary),
      headline6: TextStyle(color: $textPrimaryColor),
      subtitle2: TextStyle(color: $textSecondaryColor),
    ),
    visualDensity: VisualDensity.adaptivePlatformDensity,
  ).copyWith(
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.android: OpenUpwardsPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        TargetPlatform.linux: OpenUpwardsPageTransitionsBuilder(),
        TargetPlatform.macOS: OpenUpwardsPageTransitionsBuilder(),
      },
    ),
  );

  static final ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: $appBackgroundColorDark,
    highlightColor: $appBackgroundColorDark,
    errorColor: $errorColorDark,
    appBarTheme: AppBarTheme(
      color: $appBackgroundColorDark,
      iconTheme: const IconThemeData(
        color: $blackColor,
      ),
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
      ),
      titleTextStyle: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: FontSizes.s20,
        height: 1,
        color: $whiteColor,
      ),
    ),
    primaryColor: $colorPrimaryBlack,
    dividerColor: $dividerColorDark,
    primaryColorDark: $colorPrimaryBlack,
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: $appColorPrimary,
      selectionColor: $appColorPrimary,
      selectionHandleColor: $appColorPrimary,
    ),
    hoverColor: $hoverColorDark,
    fontFamily: Fonts.googleSans,
    bottomSheetTheme: const BottomSheetThemeData(
      backgroundColor: $appBackgroundColorDark,
    ),
    primaryTextTheme: TextTheme(
      headline6: TextStyles.body1.copyWith(color: Colors.white),
      overline: TextStyles.body1.copyWith(color: Colors.white),
    ),
    cardTheme: const CardTheme(color: $cardBackgroundBlackDark),
    cardColor: $appSecondaryBackgroundColor,
    iconTheme: const IconThemeData(color: $whiteColor),
    textTheme: const TextTheme(
      button: TextStyle(color: $colorPrimaryBlack),
      headline6: TextStyle(color: $whiteColor),
      subtitle2: TextStyle(color: $hoverColorLight),
    ),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    colorScheme: const ColorScheme.dark(
      primary: $appBackgroundColorDark,
      onPrimary: $cardBackgroundBlackDark,
    ).copyWith(
      secondary: $whiteColor,
    ),
  ).copyWith(
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.android: OpenUpwardsPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        TargetPlatform.linux: OpenUpwardsPageTransitionsBuilder(),
        TargetPlatform.macOS: OpenUpwardsPageTransitionsBuilder(),
      },
    ),
  );
}
