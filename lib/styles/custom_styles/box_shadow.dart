import 'package:flutter/material.dart';

import '../app_colors.dart';

const double $defaultBlurRadius = 4.0;
const double $defaultSpreadRadius = 1.0;

/// default box shadow
List<BoxShadow> defaultBoxShadow({
  Color? shadowColor,
  double? blurRadius,
  double? spreadRadius,
  Offset offset = const Offset(0.0, 0.0),
}) {
  return [
    BoxShadow(
      color: shadowColor ?? $shadowColorGlobal,
      blurRadius: blurRadius ?? $defaultBlurRadius,
      spreadRadius: spreadRadius ?? $defaultSpreadRadius,
      offset: offset,
    )
  ];
}
