import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'app_responsive.dart';

class ConfigScale {
  ConfigScale._initial();

  static final ConfigScale _instance = ConfigScale._initial();

  factory ConfigScale() {
    return _instance;
  }

  ScreenWidth screenWidth = ScreenWidth.DESKTOP;

  double _scale = 1;

  double get scale => _scale;

  void setConfigScale(double newValue) {
    _scale = newValue;
  }

  void setScreenWidth(ScreenWidth screen) {
    screenWidth = screen;
  }
}

class Times {
  static const Duration fastest = Duration(milliseconds: 150);
  static const fast = Duration(milliseconds: 250);
  static const medium = Duration(milliseconds: 350);
  static const slow = Duration(milliseconds: 700);
  static const slower = Duration(milliseconds: 1000);
  static const verySlower = Duration(milliseconds: 1500);
}

class Sizes {
  static double get hit => 40 * ConfigScale().scale;
}

class IconSizes {
  static double get xs => 14 * ConfigScale().scale;

  static double get sm => 18 * ConfigScale().scale;

  static double get med => 24 * ConfigScale().scale;
}

/// Padding
class Insets {
  static double get xs => 4 * ConfigScale().scale;

  static double get sm => 8 * ConfigScale().scale;

  static double get med => 12 * ConfigScale().scale;

  static double get lg => 18 * ConfigScale().scale;

  static double get xl => 24 * ConfigScale().scale;

  static double get xxl => 32 * ConfigScale().scale;

  static double get offset => 40 * ConfigScale().scale;
}

/// Border radius
class Corners {
  static double sm = 3 * ConfigScale().scale;
  static Radius smRadius = Radius.circular(sm);
  static BorderRadius smBorder = BorderRadius.all(smRadius);

  static double med = 5 * ConfigScale().scale;
  static Radius medRadius = Radius.circular(med);
  static BorderRadius medBorder = BorderRadius.all(medRadius);

  static double lg = 8 * ConfigScale().scale;
  static Radius lgRadius = Radius.circular(lg);
  static BorderRadius lgBorder = BorderRadius.all(lgRadius);

  static double xl = 12 * ConfigScale().scale;
  static Radius xlRadius = Radius.circular(xl);
  static BorderRadius xlBorder = BorderRadius.all(xlRadius);

  static double xxl = 24 * ConfigScale().scale;
  static Radius xxlRadius = Radius.circular(xxl);
  static BorderRadius xxlBorder = BorderRadius.all(xxlRadius);
}

/// Border width
class Strokes {
  static double thin = 1 * ConfigScale().scale;
  static double thick = 4 * ConfigScale().scale;
}

class Shadows {
  static List<BoxShadow> get primaryShadow => [
        BoxShadow(
          color: const Color.fromARGB(38, 27, 27, 29).withOpacity(.15),
          offset: const Offset(0, 5),
          blurRadius: 10,
        )
      ];

  static List<BoxShadow> get universal => [
        BoxShadow(
          color: const Color(0xff333333).withOpacity(.15),
          spreadRadius: 0,
          blurRadius: 10,
        ),
      ];

  static List<BoxShadow> get small => [
        BoxShadow(
          color: const Color(0xff333333).withOpacity(.15),
          spreadRadius: 0,
          blurRadius: 3,
          offset: const Offset(0, 1),
        ),
      ];
}

class FontSizes {
  static double get s10 => 10 * ConfigScale().scale;

  static double get s11 => 11 * ConfigScale().scale;

  static double get s12 => 12 * ConfigScale().scale;

  static double get s14 => 14 * ConfigScale().scale;

  static double get s16 => 16 * ConfigScale().scale;

  static double get s18 => 18 * ConfigScale().scale;

  static double get s20 => 20 * ConfigScale().scale;

  static double get s24 => 24 * ConfigScale().scale;

  static double get s48 => 48 * ConfigScale().scale;
}

class Fonts {
  static const String googleSans = "GoogleSans";
}

class TextStyles {
  static const TextStyle googleSans = TextStyle(
    fontFamily: Fonts.googleSans,
    fontWeight: FontWeight.w400,
    overflow: TextOverflow.ellipsis,
  );

  static TextStyle get h1 => googleSans.copyWith(
        fontWeight: FontWeight.w600,
        fontSize: FontSizes.s48,
        letterSpacing: -1,
        height: 1.17,
      );

  static TextStyle get h2 => h1.copyWith(
        fontSize: FontSizes.s24,
        fontWeight: FontWeight.w500,
        letterSpacing: -.5,
        height: 1.16,
      );

  static TextStyle get h3 => h1.copyWith(
        fontSize: FontSizes.s20,
        fontWeight: FontWeight.w500,
        letterSpacing: -.05,
        height: 1.29,
      );

  static TextStyle get title1 => googleSans.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: FontSizes.s18,
        height: 1.31,
      );

  static TextStyle get title2 => title1.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: FontSizes.s16,
        height: 1.36,
      );

  static TextStyle get button1 => googleSans.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: FontSizes.s16,
      );

  static TextStyle get input1 => googleSans.copyWith(
        fontSize: FontSizes.s16,
      );

  static TextStyle get body1 => googleSans.copyWith(
        fontWeight: FontWeight.normal,
        fontSize: FontSizes.s16,
        height: 1.71,
      );

  static TextStyle get body2 => body1.copyWith(
        fontSize: FontSizes.s14,
        height: 1.5,
        letterSpacing: .2,
      );

  static TextStyle get body3 => body1.copyWith(
        fontSize: FontSizes.s14,
        height: 1.5,
        fontWeight: FontWeight.bold,
      );

  static TextStyle get callOut1 => googleSans.copyWith(
        fontWeight: FontWeight.w800,
        fontSize: FontSizes.s14,
        height: 1.17,
        letterSpacing: .5,
      );

  static TextStyle get callOut2 => callOut1.copyWith(
        fontSize: FontSizes.s12,
        height: 1,
        letterSpacing: .25,
      );

  static TextStyle get caption => googleSans.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: FontSizes.s12,
        height: 1.36,
      );
}
