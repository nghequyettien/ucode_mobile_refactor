import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/constants.dart';
import 'package:ucode/extensions/extensions.dart';
import 'package:ucode/pages/sign_in/controller.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final SignInController controller = Get.put(SignInController());

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  static final double sizeIcon = 55 * ConfigScale().scale;

  double widthForm(double width) {
    if (ConfigScale().screenWidth == ScreenWidth.MOBILE) {
      return width;
    }
    return width > 500 ? 500 : width;
  }

  final Shader linearGradient = const LinearGradient(
    colors: <Color>[
      $appColorPrimary,
      $colorGradient1,
    ],
  ).createShader(
    const Rect.fromLTWH(0.0, 0.0, 0.0, 0.0),
  );

  @override
  Widget build(BuildContext context) {
    final AppStyle appStyle = context.appStyleListen;
    return Scaffold(
      body: LayoutBuilder(
        builder: (_, constrains) {
          return SingleChildScrollView(
            child: Container(
              width: widthForm(constrains.maxWidth),
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: Insets.xl),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  VSpace.xxl,
                  VSpace.xxl,
                  Text(
                    $appName,
                    style: TextStyles.h2.copyWith(
                      foreground: Paint()..shader = linearGradient,
                      fontSize: 30 * ConfigScale().scale,
                    ),
                  ),
                  VSpace.lg,
                  Text(
                    context.lang?.easy_to_learn ?? '',
                    style: TextStyles.h3.copyWith(
                      color: appStyle.isLightMode
                          ? $textPrimaryColor
                          : $whiteColor,
                    ),
                  ),
                  Text(
                    context.lang?.anywhere_and_anytime ?? '',
                    style: TextStyles.h3.copyWith(
                      color: appStyle.isLightMode
                          ? $textPrimaryColor
                          : $whiteColor,
                    ),
                  ),
                  VSpace.xl,
                  InputEditTextStyle(
                    isPassword: false,
                    hintText: context.lang?.email,
                    controller: controller.emailController,
                    prefixIcon: Icon(
                      Icons.alternate_email_rounded,
                      size: IconSizes.med,
                      color: appStyle.isLightMode
                          ? $textPrimaryColor
                          : $whiteColor,
                    ),
                  ),
                  VSpace.lg,
                  InputEditTextStyle(
                    isPassword: true,
                    hintText: context.lang?.password,
                    controller: controller.passwordController,
                    prefixIcon: Icon(
                      Icons.lock_outline_rounded,
                      size: IconSizes.med,
                      color: appStyle.isLightMode
                          ? $textPrimaryColor
                          : $whiteColor,
                    ),
                  ),
                  VSpace.lg,
                  Text(
                    context.lang?.forgot_password ?? '',
                    textAlign: TextAlign.start,
                    style: TextStyles.body1.copyWith(
                      fontWeight: FontWeight.normal,
                      color: appStyle.isLightMode
                          ? $textSecondaryColor
                          : $whiteColor,
                    ),
                  ),
                  VSpace.lg,
                  ButtonFullColor(
                    onPressed: () {
                      controller.handleSignIn(context);
                    },
                    textContent: context.lang?.sign_in,
                  ),
                  VSpace.lg,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        context.lang?.dont_you_an_account ?? '',
                        style: TextStyles.body1.copyWith(
                          fontWeight: FontWeight.normal,
                          color: appStyle.isLightMode
                              ? $textSecondaryColor
                              : $whiteColor,
                        ),
                      ),
                      HSpace.sm,
                      Text(
                        context.lang?.sign_up ?? '',
                        style: TextStyles.body1.copyWith(
                          fontWeight: FontWeight.normal,
                          color: $appColorPrimary,
                        ),
                      ),
                    ],
                  ),
                  VSpace.lg,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Divider(thickness: Strokes.thick / 2).expand(),
                      HSpace.sm,
                      Text(
                        context.lang!.or_sign_in_with,
                        style: TextStyles.body1.copyWith(
                          color: appStyle.isLightMode
                              ? $textSecondaryColor
                              : $whiteColor,
                          height: 1,
                        ),
                      ),
                      HSpace.sm,
                      Divider(thickness: Strokes.thick / 2).expand(),
                    ],
                  ),
                  VSpace.xl,
                  ButtonOutLineWithIconFlexible(
                    onPressed: () {
                      toasty(context, "Comming soon");
                    },
                    icon: Image.asset(
                      GOOGLE_IMAGE,
                      width: 20,
                      height: 20,
                    ),
                    textContent: 'Google',
                  ),
                  VSpace.xxl,
                  VSpace.xxl,
                ],
              ),
            ).center(),
          );
        },
      ),
    );
  }
}
