import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:go_router/go_router.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/utils/utils.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

class SignInController extends GetxController {
  SignInController();

  final TextEditingController emailController =
      TextEditingController(text: 'a33062@thanglong.edu.vn');
  final TextEditingController passwordController =
      TextEditingController(text: 'nghe12345');

  void handleRedirect(BuildContext context){
    if (AppState.to.isSignIn) {
      final AppRouter router = context.appRouter;
      if (router.firstPageGo.isEmpty) {
        context.goNamed(AppRouterName.$home);
      } else {
        context.go(router.firstPageGo);
      }
    }
  }

  void handleSignIn(BuildContext context) {
    if (emailController.text.isEmpty) {
      toastyError(
        context,
        'Email ${context.lang?.is_not_empty.toLowerCase()}',
      );
      return;
    }
    if (!emailController.text.validateEmail()) {
      toastyError(
        context,
        context.lang?.email_invalid ?? '',
      );
      return;
    }
    if (passwordController.text.isEmpty) {
      toastyError(
        context,
        '${context.lang?.password} ${context.lang?.is_not_empty.toLowerCase()}',
      );
      return;
    }
    if (passwordController.text.length < 6) {
      toastyError(
        context,
        context.lang?.password_invalid ?? '',
      );
      return;
    }
    Loading.show(context);
    AppState.to.signIn(
      email: emailController.text,
      password: passwordController.text,
    ).then((value){
      Loading.dismiss();
      handleRedirect(context);
    });
  }
}
