import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import 'package:shimmer/shimmer.dart';
import 'package:go_router/go_router.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/constants.dart';
import 'package:ucode/extensions/extensions.dart';

import 'controller.dart';

class CourseDetailPage extends StatefulWidget {
  const CourseDetailPage({
    Key? key,
    required this.courseId,
  }) : super(key: key);

  final String courseId;

  @override
  State<CourseDetailPage> createState() => _CourseDetailPageState();
}

class _CourseDetailPageState extends State<CourseDetailPage> {
  late final CourseDetailController courseDetailController;

  @override
  void initState() {
    super.initState();
    courseDetailController =
        Get.put(CourseDetailController(int.parse(widget.courseId)));
  }

  @override
  void dispose() {
    super.dispose();
    courseDetailController.dispose();
    Get.delete<CourseDetailController>();
  }

  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Container(
          height: 263.minusScale,
          decoration: BoxDecoration(
            image: courseDetailController.isInitLoad
                ? DecorationImage(
                    image: courseDetailController.course.cover_image != null
                        ? CachedNetworkImageProvider(
                            courseDetailController.course.cover_image!,
                          )
                        : Image.asset(DEFAULT_BANNER).image,
                    fit: BoxFit.cover)
                : null,
          ),
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(
                padding: EdgeInsets.only(
                  top: Insets.lg,
                  left: Insets.lg,
                ),
                width: context.width(),
                color: Colors.grey.withOpacity(0.1),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    VSpace.lg,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 20.minusScale,
                          backgroundColor: Colors.black12,
                          child: CloseButton(
                            onPressed: () {
                              context.pop();
                            },
                            color: $appColorPrimary,
                          ),
                        ),
                        Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            CircularPercentIndicator(
                              radius: 40.minusScale,
                              lineWidth: 3.05,
                              animation: true,
                              percent: courseDetailController.course.progress !=
                                      null
                                  ? courseDetailController.course.progress! /
                                      100
                                  : 0,
                              backgroundColor: $dividerColorDark,
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: $appColorPrimary,
                            ),
                            Text(
                              courseDetailController.course.progress
                                      ?.toInt()
                                      .toString() ??
                                  '0',
                              style: TextStyles.body1.copyWith(
                                color: $appColorPrimary,
                                fontWeight: FontWeight.w500,
                                height: 0,
                              ),
                            ),
                          ],
                        ).paddingOnly(right: Insets.lg),
                      ],
                    ),
                    VSpace.lg,
                    Text(
                      courseDetailController.course.name,
                      style: TextStyles.title1.copyWith(
                        color: $whiteColor,
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          '${courseDetailController.curriculum.length} ${context.lang!.chapter.toLowerCase()}',
                          style: TextStyles.body2.copyWith(
                            color: $whiteColor,
                          ),
                        ),
                        HSpace.lg,
                        Text(
                          '${courseDetailController.course.suggested_duration} ${context.lang!.days.toLowerCase()}',
                          style: TextStyles.body2.copyWith(
                            color: $whiteColor,
                          ),
                        ),
                        HSpace.lg,
                        Text(
                          '${courseDetailController.course.video_duration} videos',
                          style: TextStyles.body2.copyWith(
                            color: $whiteColor,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          '${courseDetailController.course.num_quizzes} ${context.lang!.lectures.toLowerCase()}',
                          style: TextStyles.body2.copyWith(
                            color: $whiteColor,
                          ),
                        ),
                        HSpace.lg,
                        Text(
                          '${courseDetailController.course.num_lectures} ${context.lang!.quizzes.toLowerCase()}',
                          style: TextStyles.body2.copyWith(
                            color: $whiteColor,
                          ),
                        ),
                      ],
                    ),
                    VSpace.xxl,
                    Container(
                      margin: EdgeInsets.only(top: Insets.med),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CachedNetworkImageCircleWidget(
                            courseDetailController.course.teacher_avatar,
                            size: 22.minusScale,
                          ),
                          HSpace.med,
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                courseDetailController.course.teacher_name ??
                                    '',
                                style: TextStyles.body1.copyWith(
                                  fontWeight: FontWeight.w500,
                                  color: $whiteColor,
                                  height: 0,
                                ),
                              ),
                              VSpace.xs,
                              Text(
                                context.lang!.teacher,
                                style: TextStyles.body2.copyWith(
                                  color: $whiteColor,
                                  height: 0,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: Insets.lg,
          top: 170.minusScale,
          bottom: 0,
          child: Container(
            height: 90.minusScale,
            width: 160.minusScale,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Corners.medRadius,
                topLeft: Corners.medRadius,
              ),
              image: courseDetailController.isInitLoad
                  ? DecorationImage(
                      image: courseDetailController.course.cover_image != null
                          ? CachedNetworkImageProvider(
                              courseDetailController.course.cover_image!,
                            )
                          : Image.asset(DEFAULT_BANNER).image,
                      fit: BoxFit.cover)
                  : null,
            ),
          ),
        ),
      ],
    );
  }

  Widget curriculumWidget() {
    return Container(
      margin: EdgeInsets.all(Insets.lg),
      width: context.width(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                context.lang!.curriculum,
                style: TextStyles.title1,
              ).expand(),
              Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () {
                      toasty(context, 'Description coming soon');
                    },
                    icon: Icon(
                      Icons.description,
                      size: IconSizes.med,
                      color: $textSecondaryColor,
                    ),
                  ),
                  HSpace.sm,
                  IconButton(
                    onPressed: () {
                      toasty(context, 'Comment coming soon');
                    },
                    icon: SvgPicture.asset(
                      IC_MESSAGE,
                      color: $textSecondaryColor,
                    ),
                  ),
                ],
              )
            ],
          ),
          VSpace.lg,
          ListView.separated(
            itemCount: courseDetailController.isInitLoad
                ? courseDetailController.curriculum.length
                : 10,
            shrinkWrap: true,
            separatorBuilder: (context, index) {
              return VSpace.lg;
            },
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              if (!courseDetailController.isInitLoad) {
                return loadingCurriculumItemWidget();
              }
              return Container(
                padding: EdgeInsets.symmetric(
                  vertical: Insets.sm,
                  horizontal: Insets.med,
                ),
                decoration: boxDecoration(
                  showShadow: true,
                  bgColor: context.cardColor,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(Insets.med),
                          decoration: BoxDecoration(
                            color: $shadowColorGlobal,
                            shape: BoxShape.circle,
                          ),
                          child: Text(
                            (index + 1).toString(),
                            style: TextStyles.body1.copyWith(height: 0),
                          ),
                        ),
                        HSpace.med,
                        Flexible(
                          child: Text(
                            courseDetailController.curriculum[index].name,
                            style: TextStyles.body1.copyWith(
                              height: 0,
                              fontWeight: FontWeight.w500,
                            ),
                            maxLines: 2,
                          ),
                        ),
                      ],
                    ).expand(),
                    CircularPercentIndicator(
                      radius: 30.minusScale,
                      lineWidth: 3,
                      animation: true,
                      percent: (courseDetailController
                                  .curriculum[index].user_percent_complete ??
                              0) /
                          100,
                      backgroundColor: $shadowColorGlobal,
                      circularStrokeCap: CircularStrokeCap.round,
                      progressColor: $appColorPrimary,
                    )
                  ],
                ).onTap(() {
                  toasty(context, 'Comming soon');
                }),
              );
            },
          )
        ],
      ),
    );
  }

  Widget loadingCurriculumItemWidget() {
    return Shimmer.fromColors(
      baseColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade400
          : Colors.black12,
      highlightColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade100
          : Colors.white12,
      child: Container(
        height: 62.minusScale,
        decoration: boxDecoration(
          showShadow: true,
          color: context.cardColor,
          borderNone: true,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Obx(
          () {
            return Column(
              children: <Widget>[
                headerWidget(),
                curriculumWidget(),
              ],
            );
          },
        ),
      ),
    );
  }
}
