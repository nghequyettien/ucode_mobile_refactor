import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

import 'state.dart';

class CourseDetailController extends GetxController {
  static CourseDetailController get to => Get.find<CourseDetailController>();

  final CourseDetailState state = CourseDetailState();

  final int courseId;

  CourseDetailController(this.courseId);

  Course get course => state.course.value;

  List<CurriculumItem> get curriculum => state.curriculum;

  bool get isInitLoad => state.isInitLoad.value;

  Future _getCourse() async {
    final Course res = await CourseEntities.getCourse(courseId);
    state.course.value = res;
  }

  Future _getCurriculum() async {
    final Curriculum res = await CurriculumEntities.getCurriculum(courseId);
    state.curriculum.value = res.data;
  }

  void _initData() async {
    await Future.wait([
      _getCourse(),
      _getCurriculum(),
    ]);
    state.isInitLoad.value = true;
  }

  @override
  void onInit() {
    _initData();
    super.onInit();
  }
}
