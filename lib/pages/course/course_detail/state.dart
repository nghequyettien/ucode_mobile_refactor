import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:ucode/models/models.dart';

class CourseDetailState {
  final RxBool isInitLoad = RxBool(false);
  final Rx<Course> course = Rx<Course>(Course.empty);
  final RxList<CurriculumItem> curriculum = RxList<CurriculumItem>([]);
}
