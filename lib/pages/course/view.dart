import 'package:flutter/material.dart';

import '../coming_soon_screen.dart';

class CoursePage extends StatefulWidget {
  const CoursePage({Key? key}) : super(key: key);

  @override
  State<CoursePage> createState() => _CoursePageState();
}

class _CoursePageState extends State<CoursePage> {
  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(namePage: 'Course',);
  }
}
