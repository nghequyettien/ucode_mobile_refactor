import 'package:flutter/material.dart';
import 'package:ucode/constants/images.dart';

import 'package:ucode/styles/styles.dart';

class ComingSoonScreen extends StatelessWidget {
  const ComingSoonScreen({Key? key, this.namePage}) : super(key: key);

  final String? namePage;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '$namePage Coming soon',
                style: TextStyles.title1,
              ),
              Container(
                height: 350 * ConfigScale().scale,
                padding: EdgeInsets.all(Insets.xxl),
                decoration: BoxDecoration(
                  color: $lightGray.withOpacity(0.5),
                  shape: BoxShape.circle,
                ),
                child: Image.asset(COMING_SOON),
              )
            ],
          ),
        ),
      ),
    );
  }
}
