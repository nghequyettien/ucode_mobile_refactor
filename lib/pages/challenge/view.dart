import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import 'package:ucode/pages/pages.dart';
import 'package:ucode/styles/styles.dart';

class ChallengePage extends StatelessWidget {
  ChallengePage({Key? key}) : super(key: key);

  final ChallengeController _challengeController =
      Get.put(ChallengeController());

  Widget _renderUI(BuildContext context) {
    switch (ConfigScale().screenWidth) {
      case ScreenWidth.DESKTOP:
        return const ChallengePageWeb();
      default:
        return ChallengePageMobile();
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return _renderUI(context);
    });
  }
}
