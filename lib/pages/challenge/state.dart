import 'package:get/state_manager.dart';

import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class ChallengeState {
  final RxBool isInitRunning = RxBool(false);
  final RxInt pageRunning = RxInt(1);
  final RxInt pageSizeRunning = RxInt(10);
  final RxList<Challenge> challengeRunning = RxList<Challenge>([]);
  final Rx<ResponseApiMetaData> metaDataRunning =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);

  final RxBool isInitNotRunning = RxBool(false);
  final RxInt pageNotRunning = RxInt(1);
  final RxInt pageSizeNotRunning = RxInt(10);
  final RxList<Challenge> challengeNotRunning = RxList<Challenge>([]);
  final Rx<ResponseApiMetaData> metaDataNotRunning =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);
}
