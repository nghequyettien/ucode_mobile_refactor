import 'package:shimmer/shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import 'package:ucode/models/models.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/utils/utils.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/constants.dart';
import 'package:ucode/extensions/extensions.dart';

import '../controller.dart';

class ChallengePageMobile extends StatelessWidget {
  ChallengePageMobile({Key? key}) : super(key: key);

  final ChallengeController _controller = ChallengeController.to;

  Widget _challengeItemWidget(
    BuildContext context, {
    bool isRunning = false,
    required String name,
    required String difficultLevel,
    String userStatus = '',
    int uCoin = 0,
    String? avatarFirstAcUser,
    String? email,
    required int startDate,
    required int endDate,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: Insets.med,
        vertical: Insets.sm,
      ),
      decoration: boxDecoration(
        bgColor: context.cardColor,
        showShadow: true,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                isRunning ? context.lang!.running : context.lang!.finished,
                style: TextStyles.body2.copyWith(
                  color: isRunning ? $appColorPrimary : $textSecondaryColor,
                ),
              ),
            ],
          ),
          VSpace.xs,
          Text(
            name.toUpperCase(),
            style: TextStyles.title1,
            maxLines: 3,
          ),
          VSpace.xs,
          difficultLevelWidget(context, difficultLevel),
          VSpace.xs,
          Text(
            '${context.lang!.start}: ${formatTimestampToTime(startDate)}',
            style: TextStyles.body2.copyWith(
              color: $textSecondaryColor,
            ),
          ),
          VSpace.xs,
          Text(
            '${context.lang!.end}: ${formatTimestampToTime(endDate)}',
            style: TextStyles.body2.copyWith(
              color: $textSecondaryColor,
            ),
          ),
          VSpace.xs,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                userStatus.isNotEmpty
                    ? context.lang!.is_finished
                    : context.lang!.is_not_finished,
                style: TextStyles.body2.copyWith(
                  color: userStatus.isNotEmpty
                      ? $appColorPrimary
                      : $textSecondaryColor,
                ),
              ),
              Flexible(
                child: UCoinWidget(
                  uCoin: uCoin,
                  iconIsRight: true,
                  isStart: false,
                ),
              ),
            ],
          ),
          VSpace.xs,
          Container(
            padding: EdgeInsets.symmetric(
              vertical: Insets.sm,
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: $warnColor,
                  width: Strokes.thin,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CachedNetworkImageCircleWidget(
                  avatarFirstAcUser,
                  size: 20 * ConfigScale().scale,
                ),
                Flexible(
                  child: Text(
                    email ?? '',
                    style: TextStyles.body1.copyWith(
                      height: 0,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ).paddingOnly(
                    right: Insets.med,
                    left: Insets.med,
                  ),
                ),
                SvgPicture.asset(
                  IC_ACHIVEMENT_SVG,
                  height: 20 * ConfigScale().scale,
                  width: 20 * ConfigScale().scale,
                ),
              ],
            ),
          )
        ],
      ),
    ).onTap(() {});
  }

  Widget _challengeItemLoading(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade400
          : Colors.black12,
      highlightColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade100
          : Colors.white12,
      child: Container(
        decoration: boxDecoration(
          showShadow: true,
          color: context.cardColor,
          borderNone: true,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: Insets.med,
          vertical: Insets.sm,
        ),
      ),
    ).paddingOnly(
      bottom: Insets.lg,
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constrains) {
        final double widthCard = constrains.maxWidth / 2;
        final double heightCard =
            ConfigScale().screenWidth == ScreenWidth.TABLET
                ? 262 * ConfigScale().scale
                : 136 * ConfigScale().scale;
        final int itemInRow =
            ConfigScale().screenWidth == ScreenWidth.TABLET ? 2 : 1;
        final double mainAxisSpacing = Insets.lg;
        final double crossAxisSpacing =
            ConfigScale().screenWidth == ScreenWidth.TABLET ? Insets.lg : 0;
        return DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(135),
              child: TabBar(
                labelPadding: const EdgeInsets.only(left: 0, right: 0),
                indicatorWeight: Insets.xs,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: $appColorPrimary,
                labelColor: $appColorPrimary,
                isScrollable: true,
                unselectedLabelColor: $iconColorSecondary,
                tabs: [
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.running,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.xs),
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.finished,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.sm),
                ],
              ).paddingOnly(left: Insets.sm, right: Insets.lg),
            ),
            body: TabBarView(
              children: <Widget>[
                LiquidPullToRefresh(
                  color: $appColorPrimary,
                  showChildOpacityTransition: false,
                  onRefresh: () async {},
                  child: Obx(() {
                    return GridView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: _controller.isInitRunning
                          ? _controller.challengeRunning.length
                          : 4,
                      padding: EdgeInsets.all(Insets.lg),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: itemInRow,
                        crossAxisSpacing: crossAxisSpacing,
                        mainAxisSpacing: _controller.isInitRunning
                            ? _controller.challengeRunning.isNotEmpty
                                ? mainAxisSpacing
                                : 0
                            : 0,
                        childAspectRatio: widthCard / heightCard,
                      ),
                      itemBuilder: (context, index) {
                        if (_controller.isInitRunning) {
                          final Challenge item =
                              _controller.challengeRunning[index];
                          return _challengeItemWidget(
                            context,
                            isRunning: true,
                            name: item.question_name,
                            difficultLevel: item.difficult_level,
                            userStatus: item.user_status ?? '',
                            uCoin: item.ucoin,
                            avatarFirstAcUser: item.first_ac_user.avatar,
                            email: item.first_ac_user.email,
                            startDate: item.start_date,
                            endDate: item.end_date,
                          );
                        }
                        return _challengeItemLoading(context);
                      },
                    );
                  }),
                ),
                LiquidPullToRefresh(
                  color: $appColorPrimary,
                  showChildOpacityTransition: false,
                  onRefresh: () async {},
                  child: Obx(() {
                    return GridView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: _controller.isInitNotRunning
                          ? _controller.challengeNotRunning.length
                          : 4,
                      padding: EdgeInsets.all(Insets.lg),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: itemInRow,
                        crossAxisSpacing: crossAxisSpacing,
                        mainAxisSpacing: _controller.isInitNotRunning
                            ? _controller.challengeNotRunning.isNotEmpty
                                ? mainAxisSpacing
                                : 0
                            : 0,
                        childAspectRatio: widthCard / heightCard,
                      ),
                      itemBuilder: (context, index) {
                        if (_controller.isInitNotRunning) {
                          final Challenge item =
                              _controller.challengeNotRunning[index];
                          return _challengeItemWidget(
                            context,
                            isRunning: false,
                            name: item.question_name,
                            difficultLevel: item.difficult_level,
                            userStatus: item.user_status ?? '',
                            uCoin: item.ucoin,
                            avatarFirstAcUser: item.first_ac_user.avatar,
                            email: item.first_ac_user.email,
                            startDate: item.start_date,
                            endDate: item.end_date,
                          );
                        }
                        return _challengeItemLoading(context);
                      },
                    );
                  }),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
