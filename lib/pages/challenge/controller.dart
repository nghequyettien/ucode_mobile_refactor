import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'package:ucode/pages/pages.dart';
import 'package:ucode/models/models.dart';
import 'package:ucode/services/entities/entities.dart';

class ChallengeController extends GetxController {
  static ChallengeController get to => Get.find<ChallengeController>();

  final ChallengeState state = ChallengeState();

  List<Challenge> get challengeRunning => state.challengeRunning;

  List<Challenge> get challengeNotRunning => state.challengeNotRunning;

  bool get isInitRunning => state.isInitRunning.value;

  bool get isInitNotRunning => state.isInitNotRunning.value;

  Future<List<Challenge>> _getChallengeDailyRunning() async {
    final Challenges res = await ChallengeEntities.getChallengeDaily(
      isRunning: true,
      page: state.pageRunning.value,
      pageSize: state.pageSizeRunning.value,
    );
    state.metaDataRunning.value = res.metadata;
    return res.challenges;
  }

  Future<List<Challenge>> _getChallengeDailyNotRunning() async {
    final Challenges res = await ChallengeEntities.getChallengeDaily(
      isRunning: false,
      page: state.pageNotRunning.value,
      pageSize: state.pageSizeNotRunning.value,
    );
    state.metaDataNotRunning.value = res.metadata;
    return res.challenges;
  }

  void _initData() {
    _getChallengeDailyRunning().then((data) {
      state.challengeRunning.value = data;
      state.isInitRunning.value = true;
    });
    _getChallengeDailyNotRunning().then((data) {
      state.challengeNotRunning.value = data;
      state.isInitNotRunning.value = true;
    });
  }

  @override
  void onInit() {
    _initData();
    super.onInit();
  }
}
