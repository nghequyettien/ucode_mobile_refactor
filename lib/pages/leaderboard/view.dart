import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/models/models.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

import 'controller.dart';
import 'widgets/leaderboard_item.dart';
import 'widgets/leaderboard_loading.dart';

class LeaderboardPage extends StatefulWidget {
  const LeaderboardPage({Key? key}) : super(key: key);

  @override
  State<LeaderboardPage> createState() => _LeaderboardPageState();
}

class _LeaderboardPageState extends State<LeaderboardPage> {
  final LeaderBoardController _leaderBoardController =
      Get.put(LeaderBoardController());

  final ConfettiController _controllerConfetti =
      ConfettiController(duration: const Duration(seconds: 5));

  @override
  void initState() {
    super.initState();
    if (!_leaderBoardController.isInitLBAll) {
      _controllerConfetti.play();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _leaderboardAllWidget(BuildContext context) {
    return LiquidPullToRefresh(
      color: $appColorPrimary,
      showChildOpacityTransition: false,
      onRefresh: () async {},
      child: Obx(
        () {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _leaderBoardController.isInitLBAll
                ? _leaderBoardController.leaderboardAll.length
                : 10,
            shrinkWrap: true,
            padding: EdgeInsets.only(
              left: Insets.lg,
              right: Insets.lg,
              top: Insets.lg,
            ),
            itemBuilder: (BuildContext context, int index) {
              if (_leaderBoardController.isInitLBAll) {
                final PersonalLeaderboard person =
                    _leaderBoardController.leaderboardAll[index];
                return LeaderboardItemWidget(
                  avatar: person.avatar,
                  fullName: person.full_name ?? '',
                  email: person.email,
                  rank: person.rank,
                  exp: person.total_exp,
                  level: person.current_exp_level,
                  uCoin: person.total_ucoin,
                );
              }
              return const LeaderboardLoadingWidget();
            },
          );
        },
      ),
    );
  }

  Widget _leaderboardDailyWidget(BuildContext context) {
    return LiquidPullToRefresh(
      color: $appColorPrimary,
      showChildOpacityTransition: false,
      onRefresh: () async {},
      child: Obx(
        () {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _leaderBoardController.isInitLBDaily
                ? _leaderBoardController.leaderboardDaily.length
                : 10,
            shrinkWrap: true,
            padding: EdgeInsets.only(
              left: Insets.lg,
              right: Insets.lg,
              top: Insets.lg,
            ),
            itemBuilder: (BuildContext context, int index) {
              if (_leaderBoardController.isInitLBDaily) {
                final PersonalLeaderboard person =
                    _leaderBoardController.leaderboardDaily[index];
                return LeaderboardItemWidget(
                  avatar: person.avatar,
                  fullName: person.full_name ?? '',
                  email: person.email,
                  rank: person.rank,
                  exp: person.daily_exp,
                  level: person.current_exp_level,
                  uCoin: person.total_ucoin,
                );
              }
              return const LeaderboardLoadingWidget();
            },
          );
        },
      ),
    );
  }

  Widget _leaderboardWeeklyWidget(BuildContext context) {
    return LiquidPullToRefresh(
      color: $appColorPrimary,
      showChildOpacityTransition: false,
      onRefresh: () async {},
      child: Obx(
        () {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _leaderBoardController.isInitLBWeekly
                ? _leaderBoardController.leaderboardWeekly.length
                : 10,
            shrinkWrap: true,
            padding: EdgeInsets.only(
              left: Insets.lg,
              right: Insets.lg,
              top: Insets.lg,
            ),
            itemBuilder: (BuildContext context, int index) {
              if (_leaderBoardController.isInitLBWeekly) {
                final PersonalLeaderboard person =
                    _leaderBoardController.leaderboardWeekly[index];
                return LeaderboardItemWidget(
                  avatar: person.avatar,
                  fullName: person.full_name ?? '',
                  email: person.email,
                  rank: person.rank,
                  exp: person.weekly_exp,
                  level: person.current_exp_level,
                  uCoin: person.total_ucoin,
                );
              }
              return const LeaderboardLoadingWidget();
            },
          );
        },
      ),
    );
  }

  Widget _leaderboardMonthlyWidget(BuildContext context) {
    return LiquidPullToRefresh(
      color: $appColorPrimary,
      showChildOpacityTransition: false,
      onRefresh: () async {},
      child: Obx(
        () {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _leaderBoardController.isInitLBMonthly
                ? _leaderBoardController.leaderboardMonthly.length
                : 10,
            shrinkWrap: true,
            padding: EdgeInsets.only(
              left: Insets.lg,
              right: Insets.lg,
              top: Insets.lg,
            ),
            itemBuilder: (BuildContext context, int index) {
              if (_leaderBoardController.isInitLBMonthly) {
                final PersonalLeaderboard person =
                    _leaderBoardController.leaderboardMonthly[index];
                return LeaderboardItemWidget(
                  avatar: person.avatar,
                  fullName: person.full_name ?? '',
                  email: person.email,
                  rank: person.rank,
                  exp: person.monthly_exp,
                  level: person.current_exp_level,
                  uCoin: person.total_ucoin,
                );
              }
              return const LeaderboardLoadingWidget();
            },
          );
        },
      ),
    );
  }

  Widget _leaderboardDailyChallengeWidget(BuildContext context) {
    return LiquidPullToRefresh(
      color: $appColorPrimary,
      showChildOpacityTransition: false,
      onRefresh: () async {},
      child: Obx(
        () {
          return ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _leaderBoardController.isInitLBWChallenges
                ? _leaderBoardController.leaderboardWeeklyChallenges.length
                : 10,
            shrinkWrap: true,
            padding: EdgeInsets.only(
              left: Insets.lg,
              right: Insets.lg,
              top: Insets.lg,
            ),
            itemBuilder: (BuildContext context, int index) {
              if (_leaderBoardController.isInitLBWChallenges) {
                final PersonalLeaderboard person =
                    _leaderBoardController.leaderboardWeeklyChallenges[index];
                return LeaderboardItemWidget(
                  avatar: person.avatar,
                  fullName: person.full_name ?? '',
                  email: person.email,
                  rank: person.rank,
                  exp: person.weekly_challenge_total_time,
                  level: person.current_exp_level,
                  uCoin: person.total_ucoin,
                );
              }
              return const LeaderboardLoadingWidget();
            },
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constrains) {
        return DefaultTabController(
          length: 5,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(135),
              child: TabBar(
                labelPadding: const EdgeInsets.only(left: 0, right: 0),
                indicatorWeight: Insets.xs,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: $appColorPrimary,
                labelColor: $appColorPrimary,
                isScrollable: true,
                unselectedLabelColor: $iconColorSecondary,
                tabs: [
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.all,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.xs),
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.daily,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.sm),
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.weekly,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.sm),
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.monthly,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.sm),
                  Container(
                    padding: EdgeInsets.all(Insets.sm),
                    child: Text(
                      context.lang!.weekly_challenges,
                      style: TextStyles.title1,
                    ),
                  ).paddingOnly(left: Insets.xs, right: Insets.sm),
                ],
              ).paddingOnly(left: Insets.sm, right: Insets.lg),
            ),
            body: Stack(
              children: <Widget>[
                TabBarView(
                  children: <Widget>[
                    _leaderboardAllWidget(context),
                    _leaderboardDailyWidget(context),
                    _leaderboardWeeklyWidget(context),
                    _leaderboardMonthlyWidget(context),
                    _leaderboardDailyChallengeWidget(context),
                  ],
                ),
                Positioned(
                  top: 30 * ConfigScale().scale,
                  left: 30 * ConfigScale().scale,
                  child: ConfettiWidget(
                    confettiController: _controllerConfetti,
                    blastDirectionality: BlastDirectionality.explosive,
                    // don't specify a direction, blast randomly
                    shouldLoop: false,
                    //
                    emissionFrequency: 0.2,
                    canvas: Size.fromRadius(
                        MediaQuery.of(context).size.height * .35),
                    colors: const [
                      Colors.redAccent,
                      Colors.tealAccent,
                      Colors.yellowAccent,
                      Colors.orange,
                      Colors.green,
                      Colors.blue,
                      Colors.pink,
                      Colors.orange,
                      Colors.purple
                    ], // manually specify the colors to be used
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
