import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'package:ucode/pages/pages.dart';
import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class LeaderBoardController extends GetxController {
  static LeaderBoardController get to => Get.find<LeaderBoardController>();

  final LeaderBoardState state = LeaderBoardState();

  List<PersonalLeaderboard> get leaderboardAll => state.leaderboardAll;

  List<PersonalLeaderboard> get leaderboardDaily => state.leaderboardDaily;

  List<PersonalLeaderboard> get leaderboardWeekly => state.leaderboardWeekly;

  List<PersonalLeaderboard> get leaderboardMonthly => state.leaderboardMonthly;

  List<PersonalLeaderboard> get leaderboardWeeklyChallenges =>
      state.leaderboardWeeklyChallenges;

  bool get isInitLBAll => state.isInitLBAll.value;

  bool get isInitLBDaily => state.isInitLBDaily.value;

  bool get isInitLBWeekly => state.isInitLBWeekly.value;

  bool get isInitLBMonthly => state.isInitLBMonthly.value;

  bool get isInitLBWChallenges => state.isInitLBWChallenges.value;

  static const String totalExp = 'total_exp';
  static const String dailyExp = 'daily_exp';
  static const String weeklyExp = 'weekly_exp';
  static const String monthlyExp = 'monthly_exp';

  Future<List<PersonalLeaderboard>> _getLBAll() async {
    final Leaderboard res = await LeaderboardEntities.getLeaderboard(
      page: state.pageLBAll.value,
      pageSize: state.pageSizeLBAll.value,
      orderBy: totalExp,
    );
    state.metaDataLBAll.value = res.metadata;
    return res.personalLeaderboards;
  }

  Future<List<PersonalLeaderboard>> _getLBDaily() async {
    final Leaderboard res = await LeaderboardEntities.getLeaderboard(
      page: state.pageLBDaily.value,
      pageSize: state.pageSizeLBDaily.value,
      orderBy: dailyExp,
    );
    state.metaDataLBDaily.value = res.metadata;
    return res.personalLeaderboards;
  }

  Future<List<PersonalLeaderboard>> _getLBWeekly() async {
    final Leaderboard res = await LeaderboardEntities.getLeaderboard(
      page: state.pageLBWeekly.value,
      pageSize: state.pageSizeLBWeekly.value,
      orderBy: weeklyExp,
    );
    state.metaDataLBWeekly.value = res.metadata;
    return res.personalLeaderboards;
  }

  Future<List<PersonalLeaderboard>> _getLBMonthly() async {
    final Leaderboard res = await LeaderboardEntities.getLeaderboard(
      page: state.pageLBMonthly.value,
      pageSize: state.pageSizeLBMonthly.value,
      orderBy: monthlyExp,
    );
    state.metaDataLBMonthly.value = res.metadata;
    return res.personalLeaderboards;
  }

  Future<List<PersonalLeaderboard>> _getLBWChallenges() async {
    final Leaderboard res = await LeaderboardEntities.getLeaderboard(
      page: state.pageLBWChallenges.value,
      pageSize: state.pageSizeLBWChallenges.value,
      orderBy: weeklyExp,
      onlyDailyChallenge: true,
    );
    state.metaDataLBWChallenges.value = res.metadata;
    return res.personalLeaderboards;
  }

  void _initData() {
    _getLBAll().then((data) {
      state.leaderboardAll.value = data;
      state.isInitLBAll.value = true;
    });
    _getLBDaily().then((data) {
      state.leaderboardDaily.value = data;
      state.isInitLBDaily.value = true;
    });
    _getLBWeekly().then((data) {
      state.leaderboardWeekly.value = data;
      state.isInitLBWeekly.value = true;
    });
    _getLBMonthly().then((data) {
      state.leaderboardMonthly.value = data;
      state.isInitLBMonthly.value = true;
    });
    _getLBWChallenges().then((data) {
      state.leaderboardWeeklyChallenges.value = data;
      state.isInitLBWChallenges.value = true;
    });
  }

  @override
  void onInit() {
    _initData();
    super.onInit();
  }
}
