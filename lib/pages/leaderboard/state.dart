import 'package:get/state_manager.dart';

import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class LeaderBoardState {
  final RxBool isInitLBAll = RxBool(false);
  final RxInt pageLBAll = RxInt(1);
  final RxInt pageSizeLBAll = RxInt(10);
  final RxList<PersonalLeaderboard> leaderboardAll =
      RxList<PersonalLeaderboard>([]);
  final Rx<ResponseApiMetaData> metaDataLBAll =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);

  final RxBool isInitLBDaily = RxBool(false);
  final RxInt pageLBDaily = RxInt(1);
  final RxInt pageSizeLBDaily = RxInt(10);
  final RxList<PersonalLeaderboard> leaderboardDaily =
      RxList<PersonalLeaderboard>([]);
  final Rx<ResponseApiMetaData> metaDataLBDaily =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);

  final RxBool isInitLBWeekly = RxBool(false);
  final RxInt pageLBWeekly = RxInt(1);
  final RxInt pageSizeLBWeekly = RxInt(10);
  final RxList<PersonalLeaderboard> leaderboardWeekly =
      RxList<PersonalLeaderboard>([]);
  final Rx<ResponseApiMetaData> metaDataLBWeekly =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);

  final RxBool isInitLBMonthly = RxBool(false);
  final RxInt pageLBMonthly = RxInt(1);
  final RxInt pageSizeLBMonthly = RxInt(10);
  final RxList<PersonalLeaderboard> leaderboardMonthly =
      RxList<PersonalLeaderboard>([]);
  final Rx<ResponseApiMetaData> metaDataLBMonthly =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);

  final RxBool isInitLBWChallenges = RxBool(false);
  final RxInt pageLBWChallenges = RxInt(1);
  final RxInt pageSizeLBWChallenges = RxInt(10);
  final RxList<PersonalLeaderboard> leaderboardWeeklyChallenges =
      RxList<PersonalLeaderboard>([]);
  final Rx<ResponseApiMetaData> metaDataLBWChallenges =
      Rx<ResponseApiMetaData>(ResponseApiMetaData.empty);
}
