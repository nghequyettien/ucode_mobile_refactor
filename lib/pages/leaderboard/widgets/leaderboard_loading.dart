import 'package:shimmer/shimmer.dart';
import 'package:flutter/material.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/extensions/extensions.dart';

class LeaderboardLoadingWidget extends StatelessWidget {
  const LeaderboardLoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade400
          : Colors.black12,
      highlightColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade100
          : Colors.white12,
      child: Container(
        height: 159.minusScale,
        decoration: boxDecoration(
          showShadow: true,
          color: context.cardColor,
          borderNone: true,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: Insets.med,
          vertical: Insets.sm,
        ),
      ),
    ).paddingOnly(bottom: Insets.lg);
  }
}
