import 'package:flutter/material.dart';

import 'package:ucode/utils/utils.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

class LeaderboardItemWidget extends StatelessWidget {
  const LeaderboardItemWidget({
    Key? key,
    this.avatar,
    required this.fullName,
    required this.email,
    required this.rank,
    required this.level,
    required this.exp,
    required this.uCoin,
  }) : super(key: key);

  final String? avatar;
  final String fullName;
  final String email;
  final int rank;
  final int level;
  final int exp;
  final int uCoin;

  Widget _renderRankWidget(int rank) {
    return Container(
      padding: EdgeInsets.all(Insets.med),
      decoration: BoxDecoration(
        color: rank == 1
            ? $errorColorLight
            : rank == 2
                ? $warnColor
                : rank == 3
                    ? $appColorPrimary
                    : Colors.transparent,
        shape: BoxShape.circle,
      ),
      child: Text(
        rank.toString(),
        style: TextStyles.title1.copyWith(
          color: (rank == 1 || rank == 2 || rank == 3)
              ? $whiteColor
              : $textSecondaryColor,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: Insets.lg),
      padding: EdgeInsets.all(Insets.med),
      decoration: boxDecoration(
        showShadow: true,
        bgColor: context.cardColor,
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    CachedNetworkImageCircleWidget(
                      avatar,
                      size: 30 * ConfigScale().scale,
                    ),
                    HSpace.med,
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            fullName,
                            style: TextStyles.body1.copyWith(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            email,
                            style: TextStyles.body1.copyWith(
                              color: $textSecondaryColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              _renderRankWidget(rank),
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(Insets.xs),
                    decoration: BoxDecoration(
                      color: $whiteColor,
                      borderRadius: Corners.medBorder,
                      border: Border.all(
                        width: Strokes.thin,
                        color: $appColorPrimary,
                      ),
                    ),
                    child: Text(
                      truncateNumber(level),
                      style: TextStyles.body1.copyWith(
                        color: $appColorPrimary,
                        height: 0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    context.lang!.level,
                    style: TextStyles.body1.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(Insets.xs),
                    decoration: BoxDecoration(
                      color: $whiteColor,
                      borderRadius: Corners.medBorder,
                      border: Border.all(
                        width: Strokes.thin,
                        color: $warnColor,
                      ),
                    ),
                    child: Text(
                      truncateNumber(uCoin),
                      style: TextStyles.body1.copyWith(
                        color: $warnColor,
                        height: 0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    "uCoin",
                    style: TextStyles.body1.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(Insets.xs),
                    decoration: BoxDecoration(
                      color: $whiteColor,
                      borderRadius: Corners.medBorder,
                      border: Border.all(
                        width: Strokes.thin,
                        color: $appColorPrimary,
                      ),
                    ),
                    child: Text(
                      truncateNumber(exp),
                      style: TextStyles.body1.copyWith(
                        color: $appColorPrimary,
                        height: 0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    "EXP",
                    style: TextStyles.body1.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
