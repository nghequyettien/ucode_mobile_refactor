import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'state.dart';

class SearchController extends GetxController{
  static SearchController get to => Get.find<SearchController>();

  final SearchState state = SearchState();

  final TextEditingController searchController = TextEditingController();
}