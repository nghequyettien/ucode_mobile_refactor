import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:ucode/styles/styles.dart';

import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

import 'controller.dart';

class SearchPage extends StatelessWidget {
  SearchPage({Key? key}) : super(key: key);

  final SearchController _challengeController = Get.put(SearchController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(
        horizontal: Insets.lg,
        vertical: Insets.med,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InputEditTextStyle(
            isPassword: false,
            hintText: context.lang?.search,
            controller: _challengeController.searchController,
            prefixIcon: Icon(
              Icons.search_rounded,
              size: IconSizes.med,
              color: $appColorPrimary,
            ),
            suffixIcon: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.clear,
                size: IconSizes.med,
                color: $textSecondaryColor,
              ),
            ),
          ),
          VSpace.lg,
          Text(
            "Top khóa học",
            style: TextStyles.title1,
          )
        ],
      ),
    );
  }
}
