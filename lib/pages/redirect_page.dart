import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/pages/pages.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/images.dart';
import 'package:ucode/extensions/extensions.dart';
import 'package:ucode/pages/my_course/index.dart';
import 'package:ucode/helpers/handle_url_path_none.dart'
    if (dart.library.html) 'package:ucode/helpers/handle_url_path.dart';

class RedirectPage extends StatefulWidget {
  const RedirectPage({
    Key? key,
    required this.initPath,
  }) : super(key: key);

  final String initPath;

  @override
  State<RedirectPage> createState() => _RedirectPageState();
}

class _RedirectPageState extends State<RedirectPage> {
  String currentPage = AppRouterName.$home;

  static final List<String> listPageDrawer = [
    AppRouterName.$home, // 0
    AppRouterName.$course, // 1
    AppRouterName.$myCourse, // 2
    AppRouterName.$problem, // 3
    AppRouterName.$blog, // 4
    AppRouterName.$compete, // 5
    AppRouterName.$challenge, // 6
    AppRouterName.$contest, // 7
    AppRouterName.$leaderboard, // 8
    AppRouterName.$codeIde, // 9
  ];

  static final List<String> listPageBottomBar = [
    AppRouterName.$home, // 0
    AppRouterName.$search, // 1
    AppRouterName.$leaderboard, // 2
    AppRouterName.$challenge, // 3
    AppRouterName.$setting, // 4
  ];

  String renderNameCurrentPage(String routerName) {
    switch (routerName) {
      case AppRouterName.$home:
        return context.lang?.home ?? '';
      case AppRouterName.$search:
        return context.lang?.search ?? '';
      case AppRouterName.$leaderboard:
        return context.lang?.leaderboard ?? '';
      case AppRouterName.$myCourse:
        return context.lang?.my_courses ?? '';
      case AppRouterName.$setting:
        return context.lang?.setting ?? '';
      case AppRouterName.$course:
        return context.lang?.courses ?? '';
      case AppRouterName.$problem:
        return context.lang?.problems ?? '';
      case AppRouterName.$blog:
        return 'Blogs';
      case AppRouterName.$compete:
        return context.lang?.compete ?? '';
      case AppRouterName.$challenge:
        return context.lang?.challenge ?? '';
      case AppRouterName.$contest:
        return context.lang?.contests ?? '';
      case AppRouterName.$codeIde:
        return 'Code IDE';
      default:
        return '';
    }
  }

  void handleRedirectPage(VoidCallback callback) {
    callback();
    final String path =
        AppRouterName.convertRouterNameToRouterPath(currentPage);
    handleUrlNotReloadWeb(
      path: path.isNotEmpty ? path : AppRouterPath.$homePath,
      name: currentPage,
    );
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      final String page =
          AppRouterPath.convertRouterPathToRouterName(widget.initPath);
      setState(() {
        currentPage = page.isNotEmpty ? page : AppRouterName.$home;
      });
    });
  }

  Widget renderPage() {
    switch (currentPage) {
      case AppRouterName.$home:
        return const HomePage();
      case AppRouterName.$search:
        return SearchPage();
      case AppRouterName.$leaderboard:
        return const LeaderboardPage();
      case AppRouterName.$myCourse:
        return const MyCoursePage();
      case AppRouterName.$setting:
        return const SettingPage();
      case AppRouterName.$course:
        return const CoursePage();
      case AppRouterName.$problem:
        return const ProblemPage();
      case AppRouterName.$blog:
        return const BlogPage();
      case AppRouterName.$compete:
        return const CompetePage();
      case AppRouterName.$challenge:
        return ChallengePage();
      case AppRouterName.$contest:
        return const ContestPage();
      case AppRouterName.$codeIde:
        return const CodeIdePage();
      default:
        return const HomePage();
    }
  }

  Drawer customDrawer() {
    return Drawer(
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 100 * ConfigScale().scale,
            child: DrawerHeader(
              margin: EdgeInsets.zero,
              child: Builder(
                builder: (context) {
                  return ListTile(
                    contentPadding: const EdgeInsets.only(left: 0),
                    leading: CachedNetworkImageCircleWidget(
                      AppState.to.user.avatar,
                      size: 30 * ConfigScale().scale,
                    ),
                    title: Text(
                      AppState.to.user.email,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Text(context.lang!.view_profile),
                    onTap: () {
                      if (currentPage != AppRouterName.$setting) {
                        handleRedirectPage(() {
                          setState(() {
                            currentPage = AppRouterName.$setting;
                          });
                          Scaffold.of(context).closeDrawer();
                        });
                      }
                    },
                  );
                },
              ),
            ),
          ),
          ListView.separated(
            padding: EdgeInsets.all(Insets.sm),
            separatorBuilder: (_, index) {
              return const Divider();
            },
            itemCount: listPageDrawer.length,
            itemBuilder: (context, index) {
              return Text(
                renderNameCurrentPage(listPageDrawer[index]),
                style: currentPage == listPageDrawer[index]
                    ? TextStyles.title1.copyWith(
                        fontWeight: FontWeight.w500,
                      )
                    : TextStyles.title2.copyWith(
                        color: $textSecondaryColor,
                      ),
              ).paddingAll(Insets.sm).onTap(() {
                if (currentPage != listPageDrawer[index]) {
                  handleRedirectPage(() {
                    setState(() {
                      currentPage = listPageDrawer[index];
                    });
                    Scaffold.of(context).closeDrawer();
                  });
                }
              });
            },
          ).expand()
        ],
      ),
    );
  }

  AppBar customAppBar() {
    return AppBar(
      title: Text(
        renderNameCurrentPage(currentPage),
        style: TextStyles.title1,
      ),
      leading: Builder(
        builder: (context) {
          return IconButton(
            icon: SvgPicture.asset(
              IC_MENU,
              width: IconSizes.med,
              height: IconSizes.med,
              color: context.appStyleListen.isLightMode
                  ? $textPrimaryColor
                  : $whiteColor,
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          );
        },
      ),
      elevation: 0,
      shadowColor: Colors.transparent,
    );
  }

  Widget customBottomNavBar() {
    final int findIndex =
        listPageBottomBar.indexWhere((page) => page == currentPage);
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: $shadowColorGlobal,
            offset: Offset.fromDirection(3, 1),
            spreadRadius: 1,
            blurRadius: 5,
          ),
        ],
      ),
      child: UCodeBottomNavigationBar(
        items: const <UCodeBottomNavigationBarItem>[
          UCodeBottomNavigationBarItem(icon: IC_HOME),
          UCodeBottomNavigationBarItem(icon: IC_SEARCH),
          UCodeBottomNavigationBarItem(icon: IC_CHART),
          UCodeBottomNavigationBarItem(icon: IC_CUP),
          UCodeBottomNavigationBarItem(icon: IC_MORE),
        ],
        currentIndex: findIndex > 0 ? findIndex : 0,
        unselectedIconTheme: IconThemeData(
          color: $iconColorSecondary,
          size: IconSizes.med,
        ),
        selectedIconTheme: IconThemeData(
          color: $appColorPrimary,
          size: IconSizes.med,
        ),
        onTap: (index) {
          if (currentPage != listPageBottomBar[index]) {
            handleRedirectPage(() {
              setState(() {
                currentPage = listPageBottomBar[index];
              });
            });
          }
        },
        type: UCodeBottomNavigationBarType.fixed,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(),
      drawer: customDrawer(),
      bottomNavigationBar: customBottomNavBar(),
      body: SafeArea(
        child: renderPage(),
      ),
    );
  }
}
