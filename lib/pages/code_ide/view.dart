import 'package:flutter/material.dart';

import 'package:ucode/pages/pages.dart';

class CodeIdePage extends StatelessWidget {
  const CodeIdePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(
      namePage: 'Code ide',
    );
  }
}
