import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/utils/utils.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/constants.dart';
import 'package:ucode/extensions/extensions.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  Widget _award(int exp) {
    Color bgColor = $appColorPrimary;
    if (exp > 10000) {
      bgColor = $warnColor;
    } else if (exp > 20000) {
      bgColor = $errorColorDark;
    }
    return Container(
      margin: EdgeInsets.only(left: Insets.sm),
      width: 35 * ConfigScale().scale,
      height: 35 * ConfigScale().scale,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: bgColor,
      ),
      child: Padding(
        padding: EdgeInsets.all(Insets.sm),
        child: Image.asset(
          IC_ACHIVEMENT,
        ),
      ),
    );
  }

  Widget _heading({
    required BuildContext context,
    required String icon,
    required String heading,
    bool iconIsSvg = true,
  }) {
    return Row(
      children: <Widget>[
        Container(
          decoration: boxDecoration(
            bgColor:
                context.appStyleListen.isLightMode ? $whiteColor : $blackColor,
            radius: Corners.lg,
            showShadow: true,
          ),
          width: 40 * ConfigScale().scale,
          height: 40 * ConfigScale().scale,
          padding: EdgeInsets.all(Insets.sm),
          child: iconIsSvg
              ? SvgPicture.asset(icon)
              : Image.asset(
                  icon,
                  color: $appColorPrimary,
                ),
        ),
        HSpace.med,
        Text(
          heading,
          style: TextStyles.body1.copyWith(height: 0),
        ),
      ],
    );
  }

  Widget _option({
    required BuildContext context,
    required String icon,
    required String heading,
    required Widget iconWidget,
    bool iconIsSvg = true,
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Insets.med,
        vertical: Insets.sm,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          _heading(
            context: context,
            icon: icon,
            heading: heading,
            iconIsSvg: iconIsSvg,
          ),
          iconWidget,
        ],
      ),
    );
  }

  Widget _simpleOption({
    required BuildContext context,
    required String icon,
    required String heading,
    bool iconIsSvg = true,
  }) {
    return _option(
      context: context,
      icon: icon,
      heading: heading,
      iconIsSvg: iconIsSvg,
      iconWidget: const Icon(
        Icons.keyboard_arrow_right,
        color: $textSecondaryColor,
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: Insets.med,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: $whiteColor, width: Strokes.thin),
            ),
            child: CachedNetworkImageCircleWidget(
              AppState.to.user.avatar,
              size: 70 * ConfigScale().scale,
            ),
          ),
          HSpace(Insets.lg),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppState.to.user.full_name ?? '',
                style: TextStyles.title2.copyWith(
                  overflow: TextOverflow.ellipsis,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              VSpace(Insets.xs),
              Text(
                AppState.to.user.email,
                style: TextStyles.title2.copyWith(
                  color: $textSecondaryColor,
                  fontWeight: FontWeight.normal,
                ),
              ),
              VSpace(Insets.sm),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ChipWidget(
                      '${context.lang!.level} ${AppState.to.user.current_exp_level}'),
                  HSpace.sm,
                  ChipOutLineWidget(
                    AppState.to.user.role == $teacher
                        ? context.lang!.teacher
                        : context.lang!.student,
                  ),
                ],
              ),
              VSpace(Insets.sm),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "${truncateNumber(AppState.to.user.total_exp)} EXP",
                    style: TextStyles.title2,
                  ),
                  _award(AppState.to.user.total_exp)
                ],
              )
            ],
          ).expand(),
        ],
      ),
    );
  }

  Widget _showUCoin() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "uCoin:",
          style: TextStyles.title1,
        ),
        VSpace.xs,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Text(
                truncateNumber(AppState.to.user.total_ucoin),
                overflow: TextOverflow.ellipsis,
                style: TextStyles.title1,
              ),
            ),
            ImageAssetWidget(
              APP_UCOIN,
              width: 30 * ConfigScale().scale,
              height: 30 * ConfigScale().scale,
            ),
          ],
        ),
        VSpace.sm,
        Divider(
          height: Strokes.thin,
        )
      ],
    );
  }

  Widget _theme(BuildContext context) {
    return _option(
      context: context,
      icon: context.appStyleListen.isLightMode ? IC_LIGHT_MODE : IC_DARK_MODE,
      heading:
          '${context.lang!.theme} (${!context.appStyleListen.isLightMode ? context.lang!.light_mode : context.lang!.dark_mode})',
      iconWidget: Switch(
        value: !context.appStyleListen.isLightMode,
        activeColor: $appColorPrimary,
        onChanged: (s) {
          if (context.appStyle.isLightMode) {
            context.appStyle.setAppThemeMode(AppThemeMode.dark);
          } else {
            context.appStyle.setAppThemeMode(AppThemeMode.light);
          }
        },
      ),
    );
  }

  Widget _language(BuildContext context) {
    return _option(
      context: context,
      icon: IC_LANGUAGE,
      heading:
          '${context.lang!.language} (${!context.appLocale.isEn ? context.lang!.vietnamese : context.lang!.english})',
      iconWidget: Switch(
        value: context.appLocale.isEn,
        activeColor: $appColorPrimary,
        onChanged: (s) {
          if (context.appLocale.isEn) {
            context.appLocale.setAppLocale(const Locale('vi'));
          } else {
            context.appLocale.setAppLocale(const Locale('en'));
          }
        },
      ),
    );
  }

  Widget _general(BuildContext context) {
    return Container(
      decoration: boxDecoration(
        bgColor: context.cardColor,
        showShadow: true,
        radius: 0,
      ),
      child: Column(
        children: <Widget>[
          _theme(context),
          _language(context),
        ],
      ),
    );
  }

  Widget _user(BuildContext context) {
    return Container(
      decoration: boxDecoration(
        bgColor: context.cardColor,
        showShadow: true,
        radius: 0,
      ),
      child: Column(
        children: <Widget>[
          _simpleOption(
            context: context,
            icon: IC_USER,
            heading: context.lang!.user_info,
          ).onTap(() {
            toasty(context, 'Coming soon');
          }),
          _simpleOption(
            context: context,
            icon: IC_REFER,
            heading: context.lang!.reference_code,
          ).onTap(() {
            toasty(context, 'Coming soon');
          }),
          _simpleOption(
            context: context,
            icon: IC_KEY,
            heading: context.lang!.change_password,
          ).onTap(() {
            toasty(context, 'Coming soon');
          }),
          _simpleOption(
            context: context,
            icon: IC_LOGOUT,
            heading: context.lang!.logout,
          ).onTap(() {
            AppState.to.logout();
            context.goNamed(AppRouterName.$signIn);
          }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _header(context).paddingOnly(left: Insets.med, right: Insets.med),
          VSpace.lg,
          _showUCoin().paddingOnly(left: Insets.med, right: Insets.med),
          VSpace.lg,
          Text(
            context.lang!.general,
            style: TextStyles.title1,
          ).paddingOnly(left: Insets.med, right: Insets.med),
          VSpace.med,
          _general(context),
          VSpace.lg,
          Text(
            context.lang!.user,
            style: TextStyles.title1,
          ).paddingOnly(left: Insets.med, right: Insets.med),
          VSpace.med,
          _user(context),
          VSpace.lg,
        ],
      ),
    );
  }
}
