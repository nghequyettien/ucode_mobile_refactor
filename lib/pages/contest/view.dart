import 'package:flutter/material.dart';
import 'package:ucode/pages/pages.dart';

class ContestPage extends StatelessWidget {
  const ContestPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(namePage: 'Contest',);
  }
}
