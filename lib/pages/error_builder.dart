import 'package:flutter/material.dart';

class ErrorBuilderPage extends StatelessWidget {
  ErrorBuilderPage({
    Key? key,
    required this.error,
  }) : super(key: key) {
    message = error.toString();
  }

  final Exception? error;
  late final String message;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(message),
      ),
    );
  }
}
