import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/pages/pages.dart';
import 'package:ucode/models/models.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String currentPage = AppRouterPath.$homePath;

  final HomeController homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constrains) {
        return SingleChildScrollView(
          child: Obx(
            () {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Hi, ${AppState.to.user.full_name}!',
                    style: TextStyles.title1,
                  ).paddingOnly(left: Insets.lg),
                  Text(
                    context.lang!.what_to_learn_today,
                    style: TextStyles.body1.copyWith(
                      color: $textSecondaryColor,
                    ),
                  ).paddingOnly(left: Insets.lg),
                  SizedBox(
                    height: 235.minusScale,
                    child: ListView.separated(
                      padding: EdgeInsets.all(Insets.lg),
                      scrollDirection: Axis.horizontal,
                      itemCount: homeController.isInitLoad
                          ? homeController.enrolledCourses.length
                          : 4,
                      itemBuilder: (context, index) {
                        if (homeController.isInitLoad) {
                          final Course item =
                              homeController.enrolledCourses[index];
                          return EnrolledCourseItemWidget(
                            id: item.id,
                            imageCourse: item.cover_image,
                            nameCourse: item.name,
                            avatarTeacher: item.teacher_avatar,
                            nameTeacher: item.teacher_name ?? '',
                            process: item.progress ?? 0,
                          );
                        }
                        return const LoadingEnrolledCourseItemWidget();
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return HSpace.lg;
                      },
                    ),
                  ),
                  VSpace.med,
                  Text(
                    context.lang!.recent_events,
                    style: TextStyles.title1,
                  ).paddingOnly(left: Insets.lg),
                  SliderWidget(
                    height: Size(context.width(), context.width() / 1.5).height,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,
                    items: HomeController.banners.map((slider) {
                      return ClipRRect(
                        borderRadius: Corners.medBorder,
                        child: Image.asset(
                          slider,
                          height: Size(context.width(), context.width() / 1.5)
                              .height,
                          width: Size(context.width(), context.width() / 1.5)
                              .width,
                          fit: BoxFit.fill,
                        ),
                      );
                    }).toList(),
                  ),
                  VSpace.med,
                  Text(
                    context.lang!.featured_course,
                    style: TextStyles.title1,
                  ).paddingOnly(left: Insets.lg),
                  SizedBox(
                    height: 260.minusScale,
                    child: ListView.separated(
                      padding: EdgeInsets.all(Insets.lg),
                      scrollDirection: Axis.horizontal,
                      itemCount: homeController.isInitLoad
                          ? homeController.courses.length
                          : 4,
                      itemBuilder: (context, index) {
                        if (homeController.isInitLoad) {
                          final Course item = homeController.courses[index];
                          return CourseItemWidget(
                            id: item.id,
                            imageCourse: item.cover_image,
                            nameCourse: item.name,
                            avatarTeacher: item.teacher_avatar,
                            nameTeacher: item.teacher_name ?? '',
                            des: item.short_description,
                          );
                        }
                        return const LoadingCourseItemWidget();
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return HSpace.lg;
                      },
                    ),
                  ),
                  VSpace.med,
                  Text(
                    context.lang!.featured_blog,
                    style: TextStyles.title1,
                  ).paddingOnly(left: Insets.lg),
                  SizedBox(
                    height: 198.minusScale,
                    child: ListView.separated(
                      padding: EdgeInsets.all(Insets.lg),
                      scrollDirection: Axis.horizontal,
                      itemCount: homeController.isInitLoad
                          ? homeController.blogs.length
                          : 4,
                      itemBuilder: (context, index) {
                        if (homeController.isInitLoad) {
                          final Blog item = homeController.blogs[index];
                          return BlogItemWidget(
                            nameBlog: item.title,
                            avatarOwner: item.user_avatar,
                            nameOwner: item.user_mame ?? 'uCode Team',
                          );
                        }
                        return const LoadingBlogItemWidget();
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return HSpace.lg;
                      },
                    ),
                  ),
                  VSpace.sm,
                ],
              );
            },
          ),
        );
      },
    );
  }
}
