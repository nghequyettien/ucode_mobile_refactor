import 'package:flutter/material.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

class BlogItemWidget extends StatelessWidget {
  BlogItemWidget({
    Key? key,
    required this.nameBlog,
    this.avatarOwner,
    required this.nameOwner,
  }) : super(key: key);

  final String nameBlog;
  final String? avatarOwner;
  final String nameOwner;

  final double _widthCard = 270.minusScale;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Insets.med),
      width: _widthCard,
      decoration: boxDecoration(
        showShadow: true,
        bgColor: context.cardColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            nameBlog,
            style: TextStyles.title1.copyWith(
              fontSize: TextStyles.title2.fontSize,
            ),
            maxLines: 4,
          ),
          VSpace.sm,
          Row(
            children: <Widget>[
              CachedNetworkImageCircleWidget(
                avatarOwner,
                size: 20.minusScale,
              ),
              HSpace.sm,
              Text(nameOwner),
            ],
          )
        ],
      ),
    ).onTap(() {
      toasty(context, 'Comming soon');
    });
  }
}
