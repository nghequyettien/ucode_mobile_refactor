import 'package:shimmer/shimmer.dart';
import 'package:flutter/material.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/extensions/extensions.dart';

class LoadingBlogItemWidget extends StatelessWidget {
  const LoadingBlogItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade400
          : Colors.black12,
      highlightColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade100
          : Colors.white12,
      child: Container(
        height: 162.minusScale,
        width: 288.minusScale,
        decoration: boxDecoration(
          showShadow: true,
          color: context.cardColor,
          borderNone: true,
        ),
      ),
    );
  }
}

class LoadingCourseItemWidget extends StatelessWidget {
  const LoadingCourseItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade400
          : Colors.black12,
      highlightColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade100
          : Colors.white12,
      child: Container(
        height: 224.minusScale,
        width: 318.minusScale,
        decoration: boxDecoration(
          showShadow: true,
          color: context.cardColor,
          borderNone: true,
        ),
      ),
    );
  }
}

class LoadingEnrolledCourseItemWidget extends StatelessWidget {
  const LoadingEnrolledCourseItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade400
          : Colors.black12,
      highlightColor: context.appStyleListen.isLightMode
          ? Colors.grey.shade100
          : Colors.white12,
      child: Container(
        height: 214.minusScale,
        width: 190.minusScale,
        decoration: boxDecoration(
          showShadow: true,
          color: context.cardColor,
          borderNone: true,
        ),
      ),
    );
  }
}
