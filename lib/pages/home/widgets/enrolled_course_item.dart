import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:ucode/router/router.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/constants/images.dart';
import 'package:ucode/extensions/extensions.dart';

class EnrolledCourseItemWidget extends StatelessWidget {
  EnrolledCourseItemWidget({
    Key? key,
    required this.id,
    this.imageCourse,
    required this.nameCourse,
    this.avatarTeacher,
    required this.nameTeacher,
    required this.process,
  }) : super(key: key);

  final int id;
  final String? imageCourse;
  final String nameCourse;
  final String? avatarTeacher;
  final String nameTeacher;
  final double process;

  final double _heightImage = 110.minusScale;
  final double _widthCard = 196.minusScale;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _widthCard,
      decoration: boxDecoration(
        showShadow: true,
        bgColor: context.cardColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(Corners.med),
              topRight: Radius.circular(Corners.med),
            ),
            child: imageCourse != null
                ? CachedNetworkImageWidget(
                    imageCourse,
                    height: _heightImage,
                    width: _widthCard,
                  )
                : Image.asset(
                    DEFAULT_BANNER,
                    height: _heightImage,
                    width: _widthCard,
                  ),
          ),
          Padding(
            padding: EdgeInsets.all(Insets.med),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  nameCourse,
                  style: TextStyles.title1.copyWith(
                    fontSize: TextStyles.title2.fontSize,
                  ),
                  maxLines: 2,
                ),
                VSpace.med,
                LinearProgressIndicator(
                  value: process / 100,
                  backgroundColor: $textSecondaryColor.withOpacity(0.2),
                  valueColor:
                      const AlwaysStoppedAnimation<Color>($appColorPrimary),
                )
              ],
            ),
          ),
        ],
      ),
    ).onTap(() {
      context.pushNamed(AppRouterName.$courseDetail, params: {
        'courseId': id.toString(),
      });
    });
  }
}
