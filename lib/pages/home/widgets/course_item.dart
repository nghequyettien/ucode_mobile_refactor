import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:ucode/constants/images.dart';

import 'package:ucode/styles/styles.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/widgets/widgets.dart';
import 'package:ucode/extensions/extensions.dart';

class CourseItemWidget extends StatelessWidget {
  CourseItemWidget({
    Key? key,
    required this.id,
    this.imageCourse,
    required this.nameCourse,
    this.avatarTeacher,
    required this.nameTeacher,
    required this.des,
  }) : super(key: key);

  final int id;
  final String? imageCourse;
  final String nameCourse;
  final String? avatarTeacher;
  final String nameTeacher;
  final String des;

  final double _heightImage = 81.minusScale;
  final double _widthImage = 141.minusScale;
  final double _widthCard = 300.minusScale;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Insets.med),
      width: _widthCard,
      decoration: boxDecoration(
        showShadow: true,
        bgColor: context.cardColor,
      ),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: Corners.medBorder,
                child: imageCourse != null
                    ? CachedNetworkImageWidget(
                        imageCourse,
                        width: _widthImage,
                        height: _heightImage,
                      )
                    : Image.asset(
                        DEFAULT_BANNER,
                        width: _widthImage,
                        height: _heightImage,
                      ),
              ),
              HSpace.med,
              Flexible(
                child: Column(
                  children: <Widget>[
                    Text(
                      nameCourse,
                      style: TextStyles.title1.copyWith(
                        fontSize: TextStyles.title2.fontSize,
                      ),
                      maxLines: 3,
                    ),
                  ],
                ),
              ),
            ],
          ),
          VSpace.sm,
          Text(
            des,
            style: TextStyles.body2.copyWith(
              color: $textSecondaryColor,
            ),
            maxLines: 3,
          ),
          VSpace.sm,
          Row(
            children: <Widget>[
              CachedNetworkImageCircleWidget(
                avatarTeacher,
                size: 19.minusScale,
              ),
              HSpace.sm,
              Text(nameTeacher),
            ],
          )
        ],
      ),
    ).onTap(() {
      context.pushNamed(AppRouterName.$courseDetail, params: {
        'courseId': id.toString(),
      });
    });
  }
}
