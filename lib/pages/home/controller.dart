import 'package:get/state_manager.dart';
import 'package:get/get_instance/src/extension_instance.dart';

import 'package:ucode/models/models.dart';
import 'package:ucode/constants/images.dart';
import 'package:ucode/services/services.dart';

import 'state.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find<HomeController>();

  final HomeState state = HomeState();

  List<Course> get enrolledCourses => state.home.value.enrolled_courses;

  List<Course> get courses => state.home.value.courses;

  List<Blog> get blogs => state.home.value.blogs;

  bool get isInitLoad => state.isInitLoad.value;

  static const List<String> banners = [
    BANNER_2,
    BANNER_1,
    BANNER_3,
    BANNER_4,
  ];

  void _initData() async {
    final Home res = await HomeEntities.getHome();
    state.home.value = res;
    state.isInitLoad.value = true;
  }

  @override
  void onInit() {
    _initData();
    super.onInit();
  }
}
