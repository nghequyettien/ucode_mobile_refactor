import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:ucode/models/models.dart';

class HomeState{
  final RxBool isInitLoad = RxBool(false);
  final Rx<Home> home = Rx<Home>(Home());
}