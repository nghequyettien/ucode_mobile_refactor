import 'package:flutter/material.dart';

import '../coming_soon_screen.dart';

class ProblemPage extends StatefulWidget {
  const ProblemPage({Key? key}) : super(key: key);

  @override
  State<ProblemPage> createState() => _ProblemPageState();
}

class _ProblemPageState extends State<ProblemPage> {
  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(namePage: 'Problem',);
  }
}
