import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:go_router/go_router.dart' as go_router;

import 'package:ucode/app_state.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/constants/constants.dart';
import 'package:ucode/extensions/extensions.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController? scaleController;
  Animation<double>? scaleAnimation;

  bool a = false;
  bool c = false;
  bool d = false;
  bool e = false;
  bool secondAnim = false;

  Color boxColor = Colors.transparent;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (AppState.to.splashFinished) {
        if (AppState.to.isSignIn) {
          context.goNamed(AppRouterName.$home);
        } else {
          context.goNamed(AppRouterName.$signIn);
        }
      } else {
        loadInfoUserFromLocalStorage();
        init();
      }
    });
  }

  @override
  void dispose() {
    if (scaleController != null) {
      scaleController!.dispose();
    }
    super.dispose();
  }

  bool isDoneInitLoadUI = false;
  bool isRedirectApp = false;

  void handleRedirectApp() {
    if (isRedirectApp) return;
    isRedirectApp = true;
    if (AppState.to.isSignIn) {
      final AppRouter router = context.appRouter;
      if (router.firstPageGo.isEmpty) {
        context.goNamed(AppRouterName.$home);
      } else {
        context.go(router.firstPageGo);
      }
    } else {
      context.goNamed(AppRouterName.$signIn);
    }
  }

  void loadInfoUserFromLocalStorage() async {
    await AppState.to.loadInfoUserFromLocalStorage();
    if (isDoneInitLoadUI) {
      handleRedirectApp();
    }
  }

  void init() async {
    Timer(const Duration(milliseconds: 600), () {
      setState(() {
        boxColor = context.iconColor;
        a = true;
      });
    });
    Timer(const Duration(milliseconds: 1500), () {
      setState(() {
        boxColor = context.scaffoldBackgroundColor;
        c = true;
      });
    });
    Timer(const Duration(milliseconds: 1700), () {
      setState(() {
        e = true;
      });
    });
    Timer(const Duration(milliseconds: 3200), () {
      secondAnim = true;

      scaleController = AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 1000),
      )..forward();
      scaleAnimation = Tween<double>(
              begin: 0.0,
              end: ConfigScale().screenWidth == ScreenWidth.DESKTOP ? 25 : 12)
          .animate(scaleController!);
      setState(() {
        boxColor = context.scaffoldBackgroundColor;
        d = true;
      });
    });
    Timer(
      const Duration(milliseconds: 4200),
      () {
        secondAnim = true;
        setState(() {
          isDoneInitLoadUI = true;
        });
        if (AppState.to.splashFinished) {
          handleRedirectApp();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double h = context.height();
    double w = context.width();
    return Scaffold(
      backgroundColor: context.scaffoldBackgroundColor,
      body: Center(
        child: Column(
          children: [
            AnimatedContainer(
              duration: Duration(milliseconds: d ? 900 : 2500),
              curve: d ? Curves.fastLinearToSlowEaseIn : Curves.elasticOut,
              height: d
                  ? 0
                  : a
                      ? h / 2.5
                      : 20,
              width: 20,
            ),
            AnimatedContainer(
              duration: Duration(seconds: c ? 2 : 0),
              curve: Curves.fastLinearToSlowEaseIn,
              height: d
                  ? h
                  : c
                      ? 130
                      : 20,
              width: d
                  ? w
                  : c
                      ? 130
                      : 20,
              decoration: BoxDecoration(
                color: c
                    ? boxColor
                    : a
                        ? Colors.green
                        : boxColor,
                borderRadius:
                    d ? const BorderRadius.only() : BorderRadius.circular(30),
              ),
              child: secondAnim
                  ? Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: context.appStyle.isLightMode
                            ? $appSplashSecondaryColor
                            : context.cardColor,
                        shape: BoxShape.circle,
                      ),
                      child: AnimatedBuilder(
                        animation: scaleAnimation!,
                        builder: (c, child) => Transform.scale(
                          scale: scaleAnimation!.value,
                          child: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: context.appStyle.isLightMode
                                  ? $appSplashSecondaryColor
                                  : context.cardColor,
                            ),
                          ),
                        ),
                      ),
                    ).center()
                  : Padding(
                      padding: const EdgeInsets.all(10),
                      child: Center(
                        child: e
                            ? Image.asset(
                                APP_IOCN_REMOVE_BG,
                                height: 130,
                                width: 130,
                              )
                            : const SizedBox(),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
