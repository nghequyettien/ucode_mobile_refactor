import 'package:flutter/material.dart';
import 'package:ucode/pages/pages.dart';

class BlogPage extends StatelessWidget {
  const BlogPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(namePage: 'Blog',);
  }
}
