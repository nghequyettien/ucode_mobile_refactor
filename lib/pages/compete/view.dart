import 'package:flutter/material.dart';
import 'package:ucode/pages/pages.dart';

class CompetePage extends StatelessWidget {
  const CompetePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(namePage: 'Compete',);
  }
}
