import 'package:flutter/material.dart';

import '../coming_soon_screen.dart';

class MyCoursePage extends StatelessWidget {
  const MyCoursePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ComingSoonScreen(namePage: 'My course',);
  }
}
