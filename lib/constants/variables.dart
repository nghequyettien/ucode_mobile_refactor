const $userToken = "USER_TOKEN";
const $appTheme = "APP_THEME";
const $appLocal = "APP_LOCAL";

const $teacher = "teacher";
const $student = "student";

const $trivial = "trivial";
const $easy = "easy";
const $medium = "medium";
const $hard = "hard";