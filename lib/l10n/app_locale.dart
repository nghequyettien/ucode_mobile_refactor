import 'package:flutter/material.dart';

import 'package:ucode/constants/constants.dart';
import 'package:ucode/helpers/helpers.dart';

class AppLocale extends ChangeNotifier {
  Locale _locale = const Locale('vi');

  Locale get locale => _locale;

  bool get isEn => locale.languageCode == 'en';

  Future _loadAppLocalFromLocalStorage() async {
    final String getLocal = await CustomSharedPreferences.get($appLocal);
    if (getLocal.isNotEmpty) {
      _locale = Locale(getLocal);
      notifyListeners();
    }
  }

  void initApp() {
    _loadAppLocalFromLocalStorage();
  }

  void setAppLocale(Locale lang) {
    _locale = lang;
    CustomSharedPreferences.set($appLocal, lang.languageCode);
    notifyListeners();
  }
}
