import 'package:flutter/material.dart';

import 'app.dart';
import 'global.dart';

void main() async {
  Global.isDev = false;
  await Global.init();
  runApp(const MyApp());
}
