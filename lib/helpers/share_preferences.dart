import 'package:shared_preferences/shared_preferences.dart';

class CustomSharedPreferences {
  static Future<SharedPreferences> getPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  static Future<String> get(String key) async {
    final SharedPreferences prefs = await getPrefs();
    return prefs.getString(key) ?? '';
  }

  static Future set(String key, String value) async {
    final SharedPreferences prefs = await getPrefs();
    prefs.setString(key, value);
  }

  static Future delete(String key) async {
    final SharedPreferences prefs = await getPrefs();
    prefs.remove(key);
  }
}
