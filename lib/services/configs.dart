/// Domain url
const String DOMAIN = 'https://ucodeapis.com/';

/// Config api request
const CACHE_ENABLE = false;

/// Error
const int EXCEPTION_ERROR = -1;