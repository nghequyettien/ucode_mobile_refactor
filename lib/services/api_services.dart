import 'api.dart';
import 'api_path.dart';
import 'response_api.dart';

class ApiServices {
  /// GET
  static Future<ResponseApi> getInfoUser() async {
    final Map<String, dynamic> response = await Api().get(
      ApiPath.INFO_USER,
    );
    return ResponseApi.fromJson(response);
  }

  static Future<ResponseApi> getChallengeDaily({
    bool? isRunning,
    required int page,
    required int pageSize,
  }) async {
    final Map<String, dynamic> response = await Api().get(
      ApiPath.CHALLENGE_DAILY,
      queryParameters: {
        'is_running': isRunning,
        'page': page,
        'pageSize': pageSize,
      },
    );
    return ResponseApi.fromJson(response);
  }

  static Future<ResponseApi> getLeaderboard({
    required int page,
    required int pageSize,
    String orderBy = 'total_exp',
    int findMe = 0,
    bool onlyDailyChallenge = false,
  }) async {
    final Map<String, dynamic> response = await Api().get(
      ApiPath.PERSONAL_LEADER_BOARD,
      queryParameters: {
        'page': page,
        'pageSize': pageSize,
        'order_desc_by': orderBy,
        'is_find_me': findMe,
        'is_only_get_daily_challenges': onlyDailyChallenge,
      },
    );
    return ResponseApi.fromJson(response);
  }

  static Future<ResponseApi> getHome() async {
    final Map<String, dynamic> response = await Api().get(
      ApiPath.HOME,
    );
    return ResponseApi.fromJson(response);
  }

  static Future<ResponseApi> getCurriculum(int id) async {
    final Map<String, dynamic> response = await Api().get(
      '${ApiPath.CURRICULUM}/$id/course-items',
    );
    return ResponseApi.fromJson(response);
  }

  static Future<ResponseApi> getCourse(int id) async {
    final Map<String, dynamic> response = await Api().get(
      '${ApiPath.COURSES}/$id',
    );
    return ResponseApi.fromJson(response);
  }

  /// --------------------------------------------------------------------------

  /// POST
  static Future<ResponseApi> signIn(String email, String password) async {
    final Map<String, dynamic> response = await Api().post(
      ApiPath.SIGN_IN,
      data: {
        'email': email,
        'password': password,
        'tenant_id': 1,
      },
    );
    return ResponseApi.fromJson(response);
  }

  static Future<ResponseApi> logout() async {
    final Map<String, dynamic> response = await Api().post(ApiPath.LOGOUT);
    return ResponseApi.fromJson(response);
  }

  /// --------------------------------------------------------------------------
}
