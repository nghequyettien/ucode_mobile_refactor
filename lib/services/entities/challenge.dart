import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class ChallengeEntities {
  static Future<Challenges> getChallengeDaily({
    bool? isRunning,
    required int page,
    required int pageSize,
  }) async {
    final ResponseApi res = await ApiServices.getChallengeDaily(
      isRunning: isRunning,
      page: page,
      pageSize: pageSize,
    );
    final Challenges challenges = Challenges.fromJson(res.data, res.metaData);
    return challenges;
  }
}
