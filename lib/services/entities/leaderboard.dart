import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class LeaderboardEntities {
  static Future<Leaderboard> getLeaderboard({
    required int page,
    required int pageSize,
    String orderBy = 'total_exp',
    int findMe = 0,
    bool onlyDailyChallenge = false,
  }) async {
    final ResponseApi res = await ApiServices.getLeaderboard(
      page: page,
      pageSize: pageSize,
      orderBy: orderBy,
      findMe: findMe,
      onlyDailyChallenge: onlyDailyChallenge,
    );
    final Leaderboard leaderboard = Leaderboard.fromJson(res.data, res.metaData);
    return leaderboard;
  }
}
