import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class CurriculumEntities {
  static Future<Curriculum> getCurriculum(int id) async {
    final ResponseApi res = await ApiServices.getCurriculum(id);
    final Curriculum curriculum = Curriculum.fromJson(res.data);
    return curriculum;
  }
}
