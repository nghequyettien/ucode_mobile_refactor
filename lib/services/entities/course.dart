import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class CourseEntities {
  static Future<Course> getCourse(int id) async {
    final ResponseApi res = await ApiServices.getCourse(id);
    final Course course = Course.fromJson(res.data);
    return course;
  }
}
