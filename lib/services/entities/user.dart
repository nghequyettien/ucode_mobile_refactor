import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class UserEntities{
  static Future<String> signIn(String email, String password) async{
    final ResponseApi res = await ApiServices.signIn(email, password);
    return res.data as String;
  }

  static Future<User> getInfoUser() async{
    final ResponseApi res = await ApiServices.getInfoUser();
    return User.fromJson(res.data);
  }
}