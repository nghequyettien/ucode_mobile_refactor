import 'package:ucode/models/models.dart';
import 'package:ucode/services/services.dart';

class HomeEntities {
  static Future<Home> getHome() async {
    final ResponseApi res = await ApiServices.getHome();
    final Home leaderboard = Home.fromJson(res.data);
    return leaderboard;
  }
}
