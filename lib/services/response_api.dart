import 'configs.dart';

class ResponseApiMetaData {
  final int currentPage;
  final int nextPage;
  final int pageSize;
  final int? previousPage;
  final int totalItems;
  final int totalPages;

  const ResponseApiMetaData({
    required this.currentPage,
    required this.nextPage,
    required this.pageSize,
    this.previousPage,
    required this.totalItems,
    required this.totalPages,
  });

  static ResponseApiMetaData get empty => const ResponseApiMetaData(
        currentPage: 0,
        nextPage: 0,
        pageSize: 0,
        totalItems: 0,
        totalPages: 0,
      );

  factory ResponseApiMetaData.fromJson(Map<String, dynamic> json) {
    return ResponseApiMetaData(
      currentPage: json['current_page'] ?? 0,
      nextPage: json['next_page'] ?? 0,
      pageSize: json['page_size'] ?? 0,
      previousPage: json['previous_page'],
      totalItems: json['total_items'] ?? 0,
      totalPages: json['total_pages'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'current_page': currentPage,
      'next_page': nextPage,
      'page_size': pageSize,
      'previous_page': previousPage,
      'total_items': totalItems,
      'total_pages': totalPages,
    };
  }
}

class ResponseApi {
  final int errorCode;
  dynamic data;
  final ResponseApiMetaData? metaData;
  final bool success;
  final String msgCode;
  final String message;

  ResponseApi({
    required this.errorCode,
    required this.data,
    this.metaData,
    this.success = false,
    this.msgCode = '',
    this.message = '',
  });

  factory ResponseApi.fromJson(Map<String, dynamic> json) {
    return ResponseApi(
      errorCode: json['error_code'] != null && json['error_code'] == 0
          ? 0
          : EXCEPTION_ERROR,
      success: json['success'] ?? false,
      data: json['data'],
      msgCode: json['msg_code'] != null ? json['msg_code'].toString() : '',
      metaData: json['metadata'] != null
          ? ResponseApiMetaData.fromJson(
              json['metadata'] as Map<String, dynamic>)
          : null,
      message: json['message'] != null ? json['message'].toString() : '',
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'error_code': errorCode,
      'data': data,
      'success': success,
      'msg_code': msgCode,
      'metaData': metaData?.toJson(),
      'message': message,
    };
  }

  List<Object?> get props => [errorCode, data, success, msgCode, metaData];
}
