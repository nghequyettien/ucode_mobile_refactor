import 'package:dio/dio.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';

import 'package:ucode/app_state.dart';
import 'package:ucode/global.dart';
import 'package:ucode/utils/utils.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/services/services.dart';

import 'package:requests_inspector/requests_inspector.dart';

class Api {
  static final Api _instance = Api._internal();

  factory Api() => _instance;

  late Dio dio;
  CancelToken cancelToken = CancelToken();

  Api._internal() {
    BaseOptions options = BaseOptions(
      baseUrl: DOMAIN,
      connectTimeout: 10000,
      receiveTimeout: 5000,
      headers: {},
      contentType: 'application/json; charset=utf-8',
      responseType: ResponseType.json,
    );

    dio = Dio(options);

    if (Global.isDev) {
      dio.interceptors.add(RequestsInspectorInterceptor());
    }

    CookieJar cookieJar = CookieJar();
    dio.interceptors.add(CookieManager(cookieJar));
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        return handler.next(options);
      },
      onResponse: (response, handler) {
        return handler.next(response);
      },
      onError: (DioError e, handler) {
        Loading.dismiss();
        showError(e);
        return handler.next(e); //continue
      },
    ));
  }

  void showError(DioError e) {
    String errorMessage = '';
    int errorCode = EXCEPTION_ERROR;
    if (e.response != null && e.response?.data != null) {
      errorMessage = e.response?.data['message'];
      errorCode = e.response?.data['error_code'] ?? EXCEPTION_ERROR;
    } else {
      errorMessage = e.message;
      errorCode =
          e.response != null ? e.response!.statusCode! : EXCEPTION_ERROR;
    }
    print(
      'error.code -> ' + e.toString() + ', error.message -> ' + e.message,
    );
    Fluttertoast.showToast(
      msg: errorMessage,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 2,
      backgroundColor: $errorColorLight,
      textColor: $whiteColor,
      fontSize: FontSizes.s16,
    );
  }

  void cancelRequests(CancelToken token) {
    token.cancel("cancelled");
  }

  Map<String, dynamic> getAuthorizationHeader() {
    var headers = <String, dynamic>{};
    headers['Content-Type'] = 'application/json';
    headers['X-Requested-With'] = 'XMLHttpRequest';
    headers['Accept'] = 'application/json';
    if (AppState.to.token.isNotEmpty) {
      headers['access-token'] = AppState.to.token;
    }
    return headers;
  }

  Options setRequestOptions(Options requestOptions) {
    requestOptions.headers = requestOptions.headers ?? {};
    Map<String, dynamic> authorization = getAuthorizationHeader();
    requestOptions.headers!.addAll(authorization);
    return requestOptions;
  }

  Future get(
    String path, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    bool refresh = false,
    bool noCache = !CACHE_ENABLE,
    bool list = false,
    String cacheKey = '',
    bool cacheDisk = false,
  }) async {
    Options requestOptions = options ?? Options();
    requestOptions.extra ??= {};
    requestOptions.extra!.addAll({
      "refresh": refresh,
      "noCache": noCache,
      "list": list,
      "cacheKey": cacheKey,
      "cacheDisk": cacheDisk,
    });
    var response = await dio.get(
      path,
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }

  Future post(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    Options requestOptions = options ?? Options();
    var response = await dio.post(
      path,
      data: data,
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }

  Future put(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    Options requestOptions = options ?? Options();
    var response = await dio.put(
      path,
      data: data,
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }

  Future patch(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    Options requestOptions = options ?? Options();
    var response = await dio.patch(
      path,
      data: data,
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }

  Future delete(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    Options requestOptions = options ?? Options();
    var response = await dio.delete(
      path,
      data: data,
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }

  Future postForm(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    Options requestOptions = options ?? Options();
    var response = await dio.post(
      path,
      data: FormData.fromMap(data),
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }

  Future postStream(
    String path, {
    dynamic data,
    int dataLength = 0,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    Options requestOptions = options ?? Options();
    requestOptions.headers!.addAll({
      Headers.contentLengthHeader: dataLength.toString(),
    });
    var response = await dio.post(
      path,
      data: Stream.fromIterable(data.map((e) => [e])),
      queryParameters: queryParameters,
      options: setRequestOptions(requestOptions),
      cancelToken: cancelToken,
    );
    return response.data;
  }
}
