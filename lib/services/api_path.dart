class ApiPath{
  static const SIGN_IN = '/api/users/sign-in';
  static const SIGN_UP = '/api/users/sign-up';
  static const LOGOUT = '/api/users/sign-out';
  static const INFO_USER = 'api/users/info';
  static const CHALLENGE_DAILY = '/api/challenge-problem/daily';
  static const PERSONAL_LEADER_BOARD = '/api/general/personal-leader-board';
  static const HOME = '/api/home';
  static const CURRICULUM = '/api/curriculum';
  static const COURSES = '/api/courses';
}