import 'package:intl/intl.dart';

String formatTime(int second) {
  Duration duration = Duration(seconds: second);
  String twoDigits(int n) => n.toString().padLeft(2, "0");
  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  String result = "";
  if(twoDigits(duration.inHours).toString() != "00"){
    result += "${twoDigits(duration.inHours)} giờ ";
  }
  if(twoDigitMinutes != "00"){
    result += "$twoDigitMinutes phút ";
  }
  if(twoDigitSeconds != "00"){
    result += "$twoDigitSeconds giây";
  }
  return result;
}

String formatTimestampToTime(int timestamp){
  final DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  return DateFormat('dd/MM/yyyy, hh:mm a').format(dateTime);
}

String formatTimestampToDate(int timestamp){
  final DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  return DateFormat('dd/MM/yyyy').format(dateTime);
}