import 'package:flutter/material.dart';

import 'package:ucode/constants/variables.dart';
import 'package:ucode/extensions/extensions.dart';

String difficultLevelRenderText(BuildContext context,String difficultLevel){
  switch(difficultLevel){
    case $trivial:
      return context.lang!.trivial;
    case $medium:
      return context.lang!.medium;
    case $hard:
      return context.lang!.hard;
    case $easy:
      return context.lang!.easy;
    default:
      return context.lang!.trivial;
  }
}