import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:ucode/styles/styles.dart';

class Loading {
  static void show(BuildContext context) {
    FToast().init(context);
    FToast().showToast(
      child: Container(
        decoration: BoxDecoration(
          color: $appColorPrimary,
          boxShadow: Shadows.universal,
          shape: BoxShape.circle,
        ),
        padding: EdgeInsets.all(Insets.sm),
        child: CircularProgressIndicator(
          color: $whiteColor,
          strokeWidth: Strokes.thick / 2,
        ),
      ),
      gravity: ToastGravity.TOP,
      toastDuration: const Duration(
        seconds: 20,
      ),
    );
  }

  static void dismiss() {
    FToast().removeQueuedCustomToasts();
  }
}
