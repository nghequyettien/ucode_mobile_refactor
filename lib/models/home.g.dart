// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Home _$HomeFromJson(Map<String, dynamic> json) => Home(
      blog_tags: (json['blog_tags'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>)
              .map((e) => Tag.fromJson(e as Map<String, dynamic>))
              .toList())
          .toList(),
      course_tags: (json['course_tags'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>)
              .map((e) => Tag.fromJson(e as Map<String, dynamic>))
              .toList())
          .toList(),
      blogs: (json['blogs'] as List<dynamic>?)
              ?.map((e) => Blog.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      courses: (json['courses'] as List<dynamic>?)
              ?.map((e) => Course.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      enrolled_courses: (json['enrolled_courses'] as List<dynamic>?)
              ?.map((e) => Course.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$HomeToJson(Home instance) => <String, dynamic>{
      'blog_tags': instance.blog_tags,
      'blogs': instance.blogs,
      'course_tags': instance.course_tags,
      'courses': instance.courses,
      'enrolled_courses': instance.enrolled_courses,
    };
