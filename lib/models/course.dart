import 'package:json_annotation/json_annotation.dart';

part 'course.g.dart';

@JsonSerializable()
class Course {
  final int id;
  final String name;
  final int? id_teacher;
  final String? teacher_name;
  final String? teacher_avatar;
  final String short_description;
  final String? language;
  final String? cover_image;
  final String? thumbnail;
  final double price;
  final double origin_price;
  final int? discount_percent;
  final List<dynamic>? tags;
  final String status;
  final String visibility;
  final String? approval_status;
  final bool? is_enrolled;
  final String slug;
  final bool is_free;
  final String desc_format;
  final String badge;
  final double? progress;
  final bool? is_paid;
  final String? subject;
  final int? suggested_duration;
  final int? video_duration;
  final int? num_videos;
  final int? num_lectures;
  final int? num_quizzes;
  final double? pass_percentage;
  final String? certificate_template;
  final int? numStudents_enrolled;
  final int? numStudents_passed;
  final int? total_ucoin;
  final int? publish_time;
  final int? published_at;
  final String? user_status;
  final String? payment_guide;
  final int? privateUCoin_factor;
  final List<dynamic>? categories;

  const Course({
    required this.id,
    required this.name,
    this.id_teacher,
    this.teacher_name,
    this.teacher_avatar,
    required this.short_description,
    this.language,
    this.cover_image,
    this.thumbnail,
    required this.price,
    required this.origin_price,
    this.discount_percent,
    required this.tags,
    required this.status,
    required this.visibility,
    this.approval_status,
    this.is_enrolled,
    required this.slug,
    required this.is_free,
    required this.desc_format,
    required this.badge,
    this.progress,
    this.is_paid,
    this.subject,
    this.suggested_duration,
    this.video_duration,
    this.num_videos,
    this.num_lectures,
    this.num_quizzes,
    this.pass_percentage,
    this.certificate_template,
    this.numStudents_enrolled,
    this.numStudents_passed,
    this.total_ucoin,
    this.publish_time,
    this.published_at,
    this.user_status,
    this.payment_guide,
    this.privateUCoin_factor,
    this.categories,
  });

  static Course get empty => const Course(
        id: 0,
        name: "",
        short_description: "",
        price: 0,
        origin_price: 0,
        status: "",
        visibility: "",
        slug: "",
        is_free: false,
        is_paid: true,
        desc_format: "",
        badge: "",
        tags: [],
      );

  factory Course.fromJson(Map<String, dynamic> json) => _$CourseFromJson(json);

  Map<String, dynamic> toJson() => _$CourseToJson(this);
}
