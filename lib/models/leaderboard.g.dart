// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leaderboard.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PersonalLeaderboard _$PersonalLeaderboardFromJson(Map<String, dynamic> json) =>
    PersonalLeaderboard(
      avatar: json['avatar'] as String?,
      current_exp_level: json['current_exp_level'] as int,
      daily_exp: json['daily_exp'] as int,
      diff_rank: json['diff_rank'] as int,
      email: json['email'] as String,
      full_name: json['full_name'] as String?,
      id: json['id'] as int,
      monthly_exp: json['monthly_exp'] as int,
      rank: json['rank'] as int,
      role: json['role'] as String?,
      total_exp: json['total_exp'] as int,
      total_ucoin: json['total_ucoin'] as int,
      type: json['type'] as String?,
      weekly_challenge_total_time: json['weekly_challenge_total_time'] as int,
      weekly_exp: json['weekly_exp'] as int,
    );

Map<String, dynamic> _$PersonalLeaderboardToJson(
        PersonalLeaderboard instance) =>
    <String, dynamic>{
      'avatar': instance.avatar,
      'current_exp_level': instance.current_exp_level,
      'daily_exp': instance.daily_exp,
      'diff_rank': instance.diff_rank,
      'email': instance.email,
      'full_name': instance.full_name,
      'id': instance.id,
      'monthly_exp': instance.monthly_exp,
      'rank': instance.rank,
      'role': instance.role,
      'total_exp': instance.total_exp,
      'total_ucoin': instance.total_ucoin,
      'type': instance.type,
      'weekly_challenge_total_time': instance.weekly_challenge_total_time,
      'weekly_exp': instance.weekly_exp,
    };

Leaderboard _$LeaderboardFromJson(Map<String, dynamic> json) => Leaderboard(
      personalLeaderboards: (json['personalLeaderboards'] as List<dynamic>)
          .map((e) => PersonalLeaderboard.fromJson(e as Map<String, dynamic>))
          .toList(),
      metadata: ResponseApiMetaData.fromJson(
          json['metadata'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LeaderboardToJson(Leaderboard instance) =>
    <String, dynamic>{
      'personalLeaderboards': instance.personalLeaderboards,
      'metadata': instance.metadata,
    };
