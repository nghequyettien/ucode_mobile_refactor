import 'package:json_annotation/json_annotation.dart';

part 'curriculum.g.dart';

class Curriculum {
  final List<CurriculumItem> data;

  Curriculum({required this.data});

  factory Curriculum.fromJson(List<dynamic> json) {
    List<CurriculumItem> curriculum = [];
    curriculum = json
        .map<CurriculumItem>(
            (item) => CurriculumItem.fromJson(item as Map<String, dynamic>))
        .toList();
    return Curriculum(
      data: curriculum,
    );
  }

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> dataToJson = [];
    dataToJson =
        data.map<Map<String, dynamic>>((item) => item.toJson()).toList();
    return {
      'data': dataToJson,
    };
  }
}

@JsonSerializable()
class CurriculumItem {
  final int id;
  final int level;
  final String item_type;
  final String name;
  final String? content_type;
  final String? quiz_type;
  final int ucoin;
  final bool? is_preview;
  final bool? is_free;
  final int? lesson_item_id;
  final int? parent_id;
  final String status;
  final bool? is_lesson_group;
  final String? user_status;
  final int? user_started_at;
  final double? user_percent_complete;
  final int? user_last_joined_at;
  final List<CurriculumItem>? items;

  CurriculumItem({
    required this.id,
    required this.level,
    required this.item_type,
    required this.name,
    this.content_type,
    this.quiz_type,
    required this.ucoin,
    this.is_preview,
    this.is_free,
    this.lesson_item_id,
    this.parent_id,
    required this.status,
    this.is_lesson_group,
    this.user_status,
    this.user_started_at,
    this.user_percent_complete,
    this.user_last_joined_at,
    this.items,
  });

  factory CurriculumItem.fromJson(Map<String, dynamic> json) =>
      _$CurriculumItemFromJson(json);

  Map<String, dynamic> toJson() => _$CurriculumItemToJson(this);
}
