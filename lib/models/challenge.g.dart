// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'challenge.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChallengeFirstAccountUser _$ChallengeFirstAccountUserFromJson(
        Map<String, dynamic> json) =>
    ChallengeFirstAccountUser(
      id: json['id'] as int?,
      name: json['name'] as String?,
      avatar: json['avatar'] as String?,
      email: json['email'] as String?,
    );

Map<String, dynamic> _$ChallengeFirstAccountUserToJson(
        ChallengeFirstAccountUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'avatar': instance.avatar,
      'email': instance.email,
    };

Challenge _$ChallengeFromJson(Map<String, dynamic> json) => Challenge(
      id: json['id'] as int,
      question_id: json['question_id'] as int,
      question_name: json['question_name'] as String,
      start_date: json['start_date'] as int,
      end_date: json['end_date'] as int,
      status: json['status'] as String,
      ucoin: json['ucoin'] as int,
      difficult_level: json['difficult_level'] as String,
      is_running: json['is_running'] as bool,
      max_submit_times: json['max_submit_times'] as int?,
      user_status: json['user_status'] as String?,
      first_ac_user: ChallengeFirstAccountUser.fromJson(
          json['first_ac_user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChallengeToJson(Challenge instance) => <String, dynamic>{
      'id': instance.id,
      'question_id': instance.question_id,
      'question_name': instance.question_name,
      'start_date': instance.start_date,
      'end_date': instance.end_date,
      'status': instance.status,
      'ucoin': instance.ucoin,
      'difficult_level': instance.difficult_level,
      'is_running': instance.is_running,
      'max_submit_times': instance.max_submit_times,
      'user_status': instance.user_status,
      'first_ac_user': instance.first_ac_user,
    };

Challenges _$ChallengesFromJson(Map<String, dynamic> json) => Challenges(
      challenges: (json['challenges'] as List<dynamic>)
          .map((e) => Challenge.fromJson(e as Map<String, dynamic>))
          .toList(),
      metadata: ResponseApiMetaData.fromJson(
          json['metadata'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChallengesToJson(Challenges instance) =>
    <String, dynamic>{
      'challenges': instance.challenges,
      'metadata': instance.metadata,
    };
