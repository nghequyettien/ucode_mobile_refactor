// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'curriculum.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurriculumItem _$CurriculumItemFromJson(Map<String, dynamic> json) =>
    CurriculumItem(
      id: json['id'] as int,
      level: json['level'] as int,
      item_type: json['item_type'] as String,
      name: json['name'] as String,
      content_type: json['content_type'] as String?,
      quiz_type: json['quiz_type'] as String?,
      ucoin: json['ucoin'] as int,
      is_preview: json['is_preview'] as bool?,
      is_free: json['is_free'] as bool?,
      lesson_item_id: json['lesson_item_id'] as int?,
      parent_id: json['parent_id'] as int?,
      status: json['status'] as String,
      is_lesson_group: json['is_lesson_group'] as bool?,
      user_status: json['user_status'] as String?,
      user_started_at: json['user_started_at'] as int?,
      user_percent_complete:
          (json['user_percent_complete'] as num?)?.toDouble(),
      user_last_joined_at: json['user_last_joined_at'] as int?,
      items: (json['items'] as List<dynamic>?)
          ?.map((e) => CurriculumItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CurriculumItemToJson(CurriculumItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'level': instance.level,
      'item_type': instance.item_type,
      'name': instance.name,
      'content_type': instance.content_type,
      'quiz_type': instance.quiz_type,
      'ucoin': instance.ucoin,
      'is_preview': instance.is_preview,
      'is_free': instance.is_free,
      'lesson_item_id': instance.lesson_item_id,
      'parent_id': instance.parent_id,
      'status': instance.status,
      'is_lesson_group': instance.is_lesson_group,
      'user_status': instance.user_status,
      'user_started_at': instance.user_started_at,
      'user_percent_complete': instance.user_percent_complete,
      'user_last_joined_at': instance.user_last_joined_at,
      'items': instance.items,
    };
