// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'blog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Blog _$BlogFromJson(Map<String, dynamic> json) => Blog(
      id: json['id'] as int,
      title: json['title'] as String,
      user_id: json['user_id'] as int,
      user_mame: json['user_mame'] as String?,
      user_avatar: json['user_avatar'] as String?,
      cover_image: json['cover_image'] as String?,
      published_at: json['published_at'] as int,
      publish_time: json['publish_time'] as int?,
      num_likes: json['num_likes'] as int,
      num_comments: json['num_comments'] as int,
      tags: (json['tags'] as List<dynamic>?)
          ?.map((e) => Tag.fromJson(e as Map<String, dynamic>))
          .toList(),
      content_format: json['content_format'] as String,
      status: json['status'] as String,
      visibility: json['visibility'] as String,
      approval_status: json['approval_status'] as String?,
      approval_msg: json['approval_msg'] as String?,
      slug: json['slug'] as String,
      thumbnail_facebook: json['thumbnail_facebook'] as String?,
      thumbnail_twitter: json['thumbnail_twitter'] as String?,
      short_description_twitter: json['short_description_twitter'] as String?,
      short_description_facebook: json['short_description_facebook'] as String?,
      short_description: json['short_description'] as String?,
      thumbnail: json['thumbnail'] as String?,
    );

Map<String, dynamic> _$BlogToJson(Blog instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'user_id': instance.user_id,
      'user_mame': instance.user_mame,
      'user_avatar': instance.user_avatar,
      'cover_image': instance.cover_image,
      'published_at': instance.published_at,
      'publish_time': instance.publish_time,
      'num_likes': instance.num_likes,
      'num_comments': instance.num_comments,
      'tags': instance.tags,
      'content_format': instance.content_format,
      'status': instance.status,
      'visibility': instance.visibility,
      'approval_status': instance.approval_status,
      'approval_msg': instance.approval_msg,
      'slug': instance.slug,
      'thumbnail_facebook': instance.thumbnail_facebook,
      'short_description_facebook': instance.short_description_facebook,
      'thumbnail_twitter': instance.thumbnail_twitter,
      'short_description_twitter': instance.short_description_twitter,
      'short_description': instance.short_description,
      'thumbnail': instance.thumbnail,
    };
