import 'package:json_annotation/json_annotation.dart';
import 'package:ucode/services/services.dart';

part 'leaderboard.g.dart';

@JsonSerializable()
class PersonalLeaderboard{
  final String? avatar;
  final int current_exp_level;
  final int daily_exp;
  final int diff_rank;
  final String email;
  final String? full_name;
  final int id;
  final int monthly_exp;
  final int rank;
  final String? role;
  final int total_exp;
  final int total_ucoin;
  final String? type;
  final int weekly_challenge_total_time;
  final int weekly_exp;

  PersonalLeaderboard({
    this.avatar,
    required this.current_exp_level,
    required this.daily_exp,
    required this.diff_rank,
    required this.email,
    this.full_name,
    required this.id,
    required this.monthly_exp,
    required this.rank,
    this.role,
    required this.total_exp,
    required this.total_ucoin,
    this.type,
    required this.weekly_challenge_total_time,
    required this.weekly_exp,
  });

  factory PersonalLeaderboard.fromJson(Map<String, dynamic> json) =>
      _$PersonalLeaderboardFromJson(json);

  Map<String, dynamic> toJson() => _$PersonalLeaderboardToJson(this);
}

@JsonSerializable()
class Leaderboard {
  final List<PersonalLeaderboard> personalLeaderboards;
  final ResponseApiMetaData metadata;

  const Leaderboard({
    required this.personalLeaderboards,
    required this.metadata,
  });

  factory Leaderboard.fromJson(
      List<dynamic> json, ResponseApiMetaData? metadata) {
    return Leaderboard(
      personalLeaderboards: json
          .map(
            (e) => PersonalLeaderboard.fromJson(e as Map<String, dynamic>),
      )
          .toList(),
      metadata: metadata ?? ResponseApiMetaData.empty,
    );
  }

  Map<String, dynamic> toJson() => _$LeaderboardToJson(this);
}