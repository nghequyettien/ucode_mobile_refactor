import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final int id;
  final int? tenant_id;
  final String email;
  final String? full_name;
  final String? gender;
  final String? dob;
  final String type;
  final String status;
  final bool? email_verified;
  final String? address_line;
  final String? address_line_2;
  final String? phone_number;
  final bool? phone_verified;
  final String? avatar;
  final String? role;
  final String? parent_email;
  final int? parent_id;
  final String? referral_token;
  final String? api_key;
  final String? api_key_expired;
  final String? province_code;
  final String? province_name;
  final String? school;
  final String? clazz;
  final int? num_notifications;
  final int total_ucoin;
  final int current_exp_level;
  final int total_exp;
  final int? next_exp_level_start;
  final int? current_exp_level_start;
  final String? last_login;
  final bool? is_has_password;
  final bool? user_type_set;

  const User({
    required this.id,
    this.tenant_id,
    required this.email,
    this.full_name,
    this.gender,
    this.dob,
    required this.type,
    required this.status,
    this.email_verified,
    this.address_line,
    this.address_line_2,
    this.phone_number,
    this.phone_verified,
    this.avatar,
    this.role,
    this.parent_email,
    this.parent_id,
    this.referral_token,
    this.api_key,
    this.api_key_expired,
    this.province_code,
    this.province_name,
    this.school,
    this.clazz,
    this.num_notifications,
    required this.total_ucoin,
    required this.current_exp_level,
    required this.total_exp,
    this.next_exp_level_start,
    this.current_exp_level_start,
    this.last_login,
    this.is_has_password,
    this.user_type_set,
  });

  static const empty = User(
    id: 0,
    email: "",
    type: "",
    status: "",
    current_exp_level: 0,
    total_ucoin: 0,
    total_exp: 0,
  );

  bool get isEmpty => this == User.empty;

  bool get isNotEmpty => this != User.empty;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
