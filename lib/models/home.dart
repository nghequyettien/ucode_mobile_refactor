import 'package:json_annotation/json_annotation.dart';

import 'tag.dart';
import 'blog.dart';
import 'course.dart';

part 'home.g.dart';

@JsonSerializable()
class Home {
  final List<List<Tag>>? blog_tags;
  final List<Blog> blogs;
  final List<List<Tag>>? course_tags;
  final List<Course> courses;
  final List<Course> enrolled_courses;

  Home({
    this.blog_tags,
    this.course_tags,
    this.blogs = const [],
    this.courses = const [],
    this.enrolled_courses = const [],
  });

  factory Home.fromJson(Map<String, dynamic> json) =>
      _$HomeFromJson(json);

  Map<String, dynamic> toJson() => _$HomeToJson(this);
}
