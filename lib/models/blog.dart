import 'package:json_annotation/json_annotation.dart';

import 'tag.dart';

part 'blog.g.dart';

@JsonSerializable()
class Blog {
  final int id;
  final String title;
  final int user_id;
  final String? user_mame;
  final String? user_avatar;
  final String? cover_image;
  final int published_at;
  final int? publish_time;
  final int num_likes;
  final int num_comments;
  final List<Tag>? tags;
  final String content_format;
  final String status;
  final String visibility;
  final String? approval_status;
  final String? approval_msg;
  final String slug;
  final String? thumbnail_facebook;
  final String? short_description_facebook;
  final String? thumbnail_twitter;
  final String? short_description_twitter;
  final String? short_description;
  final String? thumbnail;

  Blog({
    required this.id,
    required this.title,
    required this.user_id,
    this.user_mame,
    this.user_avatar,
    this.cover_image,
    required this.published_at,
    this.publish_time,
    required this.num_likes,
    required this.num_comments,
    this.tags,
    required this.content_format,
    required this.status,
    required this.visibility,
    this.approval_status,
    this.approval_msg,
    required this.slug,
    this.thumbnail_facebook,
    this.thumbnail_twitter,
    this.short_description_twitter,
    this.short_description_facebook,
    this.short_description,
    this.thumbnail,
  });

  static Blog get emptyBlog {
    return Blog(
      id: -1,
      title: "",
      user_id: -1,
      published_at: 0,
      num_likes: 0,
      num_comments: 0,
      content_format: "",
      status: "",
      visibility: "",
      slug: "",
    );
  }

  factory Blog.fromJson(Map<String, dynamic> json) =>
      _$BlogFromJson(json);

  Map<String, dynamic> toJson() => _$BlogToJson(this);
}
