import 'package:json_annotation/json_annotation.dart';
import 'package:ucode/services/services.dart';

part 'challenge.g.dart';

@JsonSerializable()
class ChallengeFirstAccountUser {
  final int? id;
  final String? name;
  final String? avatar;
  final String? email;

  const ChallengeFirstAccountUser({
    this.id,
    this.name,
    this.avatar,
    this.email,
  });

  factory ChallengeFirstAccountUser.fromJson(Map<String, dynamic> json) =>
      _$ChallengeFirstAccountUserFromJson(json);

  Map<String, dynamic> toJson() => _$ChallengeFirstAccountUserToJson(this);
}

@JsonSerializable()
class Challenge {
  final int id;
  final int question_id;
  final String question_name;
  final int start_date;
  final int end_date;
  final String status;
  final int ucoin;
  final String difficult_level;
  final bool is_running;
  final int? max_submit_times;
  final String? user_status;
  final ChallengeFirstAccountUser first_ac_user;

  const Challenge({
    required this.id,
    required this.question_id,
    required this.question_name,
    required this.start_date,
    required this.end_date,
    required this.status,
    required this.ucoin,
    required this.difficult_level,
    required this.is_running,
    this.max_submit_times,
    this.user_status,
    required this.first_ac_user,
  });

  factory Challenge.fromJson(Map<String, dynamic> json) =>
      _$ChallengeFromJson(json);

  Map<String, dynamic> toJson() => _$ChallengeToJson(this);
}

@JsonSerializable()
class Challenges {
  final List<Challenge> challenges;
  final ResponseApiMetaData metadata;

  const Challenges({
    required this.challenges,
    required this.metadata,
  });

  factory Challenges.fromJson(
      List<dynamic> json, ResponseApiMetaData? metadata) {
    return Challenges(
      challenges: json
          .map(
            (e) => Challenge.fromJson(e as Map<String, dynamic>),
          )
          .toList(),
      metadata: metadata ?? ResponseApiMetaData.empty,
    );
  }

  Map<String, dynamic> toJson() => _$ChallengesToJson(this);
}
