import 'package:flutter/material.dart';

import 'package:requests_inspector/requests_inspector.dart';

import 'app.dart';
import 'global.dart';

void main() async {
  Global.isDev = true;
  await Global.init();
  runApp(
    const RequestsInspector(
      enabled: true,
      child: MyApp(),
    ),
  );
}
