import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:ucode/l10n/l10n.dart';
import 'package:ucode/utils/utils.dart';
import 'package:ucode/styles/styles.dart';
import 'package:ucode/router/router.dart';
import 'package:ucode/l10n/app_locale.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppStyle>(
          lazy: false,
          create: (_) => AppStyle(),
        ),
        ChangeNotifierProvider<AppLocale>(
          lazy: false,
          create: (_) => AppLocale(),
        ),
        Provider<AppRouter>(
          lazy: false,
          create: (_) => AppRouter(),
        ),
      ],
      builder: (context, child) {
        final router = Provider.of<AppRouter>(context, listen: false).router;
        final styles = Provider.of<AppStyle>(context, listen: true);
        final locale = Provider.of<AppLocale>(context, listen: true);
        return LayoutBuilder(
          builder: (_, constrains){
            onChangeScreen(constrains.maxWidth);
            return MaterialApp.router(
              title: 'uCode',
              debugShowCheckedModeBanner: true,
              routerDelegate: router.routerDelegate,
              routeInformationParser: router.routeInformationParser,
              theme: styles.themeMode,
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: L10n.all,
              locale: locale.locale,
              scrollBehavior: AppScrollBehavior(),
            );
          },
        );
      },
    );
  }
}
